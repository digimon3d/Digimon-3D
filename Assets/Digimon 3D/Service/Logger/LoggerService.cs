﻿using System;
using System.IO;
using System.Text;
using UniRx.Async;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Service.Logger
{
    internal sealed class LoggerService : IInitializable, IDisposable
    {
        private StreamWriter StreamWriter { get; set; }

        private string LoggerFilePath { get; }

        public LoggerService(string loggerFilePath)
        {
            LoggerFilePath = loggerFilePath;
        }

        public void Initialize()
        {
            StreamWriter = new StreamWriter(new FileStream(LoggerFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite), Encoding.UTF8);

            UniTaskScheduler.UnobservedExceptionWriteLogType = LogType.Exception;
            Application.logMessageReceived += ApplicationOnLogMessageReceived;

            Debug.Log("========== Digimon 3D started! ==========");
        }

        private void ApplicationOnLogMessageReceived(string condition, string stackTrace, LogType type)
        {
            if (StreamWriter == null)
                return;

            var outputBuffer = $"{DateTime.Now:s} [{type.ToString()}] {condition}";

            StreamWriter.WriteLine(outputBuffer);

            if (type == LogType.Assert || type == LogType.Error || type == LogType.Exception)
                StreamWriter.WriteLine(stackTrace);

            StreamWriter.Flush();

            if (type != LogType.Assert && type != LogType.Error && type != LogType.Exception)
                return;

            Application.Quit();
        }

        public void Dispose()
        {
            Debug.Log("========== Digimon 3D Closed! ==========");
            StreamWriter?.Dispose();
            Application.logMessageReceived -= ApplicationOnLogMessageReceived;
        }
    }
}