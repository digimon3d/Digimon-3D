﻿using System;
using System.Collections.Generic;
using Zenject;

namespace Digimon_3D.Service.GameMode
{
    internal sealed class GameModeService : IInitializable
    {
        public event Action<Model.GameMode> GameModeChangedEvent;

        public List<Model.GameMode> GameModes { get; private set; }

        public Model.GameMode ActiveGameMode { get; private set; }

        private string GameModeDirectory { get; }

        private string GameModeInfoFileFormat { get; }

        public void Initialize()
        {
            GameModes = new List<Model.GameMode>();
        }
    }
}