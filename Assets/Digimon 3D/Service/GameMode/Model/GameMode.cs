﻿using System.IO;
using UnityEngine;

namespace Digimon_3D.Service.GameMode.Model
{
    internal sealed class GameMode
    {
        public string GameModeId { get; }

        public string GameModeBasePath { get; }

        public GameModeInfo GameModeInfo { get; }

        public GameMode(string gameModeBasePath, string json)
        {
            GameModeId = Path.GetDirectoryName(gameModeBasePath);
            GameModeBasePath = gameModeBasePath;
            GameModeInfo = JsonUtility.FromJson<GameModeInfo>(json);
        }
    }
}
