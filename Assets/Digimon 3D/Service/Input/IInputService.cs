﻿namespace Digimon_3D.Service.Input
{
    public interface IInputService
    {
        bool IsKeyDown(string inputName);

        bool IsKeyHeldDown(string inputName);

        bool IsKeyUp(string inputName);
    }
}
