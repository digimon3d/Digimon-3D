﻿using System;
using System.Collections.Generic;
using Digimon_3D.Service.Setting.Input;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Service.Input
{
    internal sealed class InputService : IInputService, IInitializable, ITickable
    {
        public bool IsAnyKeyDown => IsKeyCodeDown.Count > 0;

        public bool IsAnyKeyHeldDown => IsKeyCodeHeldDown.Count > 0;

        public bool IsAnyKeyUp => IsKeyCodeUp.Count > 0;

        private IInputSettingService InputSettingsService { get; }

        private List<KeyCode> IsKeyCodeDown { get; set; }

        private List<KeyCode> IsKeyCodeHeldDown { get; set; }

        private List<KeyCode> IsKeyCodeUp { get; set; }

        private List<KeyCode> KeyCodes { get; set; }

        public InputService(IInputSettingService inputSettingService)
        {
            InputSettingsService = inputSettingService;
        }

        public void Initialize()
        {
            var keyCodes = Enum.GetValues(typeof(KeyCode));

            IsKeyCodeDown = new List<KeyCode>(8);
            IsKeyCodeHeldDown = new List<KeyCode>(8);
            IsKeyCodeUp = new List<KeyCode>(8);
            KeyCodes = new List<KeyCode>(keyCodes.Length);

            foreach (KeyCode keyCode in keyCodes)
            {
                if (keyCode == KeyCode.None)
                    continue;

                KeyCodes.Add(keyCode);
            }
        }

        public void Tick()
        {
            if (IsKeyCodeDown.Count > 0)
                IsKeyCodeDown.Clear();

            if (IsKeyCodeUp.Count > 0)
                IsKeyCodeUp.Clear();

            foreach (var key in KeyCodes)
            {
                if (UnityEngine.Input.GetKeyDown(key))
                {
                    if (!IsKeyCodeDown.Contains(key))
                        IsKeyCodeDown.Add(key);

                    if (!IsKeyCodeHeldDown.Contains(key))
                        IsKeyCodeHeldDown.Add(key);
                }
                else if (UnityEngine.Input.GetKeyUp(key))
                {
                    if (IsKeyCodeDown.Contains(key))
                        IsKeyCodeDown.Remove(key);

                    if (IsKeyCodeHeldDown.Contains(key))
                        IsKeyCodeHeldDown.Remove(key);

                    if (!IsKeyCodeUp.Contains(key))
                        IsKeyCodeUp.Add(key);
                }
            }
        }

        public bool IsKeyDown(string inputName)
        {
            return ResolveInputPressed(inputName, IsKeyCodeDown);
        }

        public bool IsKeyHeldDown(string inputName)
        {
            return ResolveInputPressed(inputName, IsKeyCodeHeldDown);
        }

        public bool IsKeyUp(string inputName)
        {
            return ResolveInputPressed(inputName, IsKeyCodeUp);
        }

        private Setting.Input.Model.Input ResolveInput(string inputName)
        {
            if (inputName == null)
                throw new ArgumentNullException(nameof(inputName));

            foreach (var input in InputSettingsService.Inputs)
            {
                if (input.Name.Equals(inputName, StringComparison.OrdinalIgnoreCase))
                    return input;
            }

            return null;
        }

        private bool ResolveInputPressed(string inputName, List<KeyCode> keyCodeIsDown)
        {
            var input = ResolveInput(inputName);

            if (input == null)
                return false;

            foreach (var assignment in input.Assignments)
            {
                var conditionResult = true;

                foreach (var condition in assignment.Conditions)
                {
                    if (keyCodeIsDown.Contains(condition))
                        continue;

                    conditionResult = false;
                    break;
                }

                if (conditionResult)
                    return true;
            }

            return false;
        }
    }
}