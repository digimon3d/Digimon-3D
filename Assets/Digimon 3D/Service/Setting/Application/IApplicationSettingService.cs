﻿using System;

namespace Digimon_3D.Service.Setting.Application
{
    public interface IApplicationSettingService
    {
        event Action<float> BackgroundMusicVolumeChangedEvent;

        event Action<float> BackgroundSoundVolumeChangedEvent;

        event Action<float> MusicEffectVolumeChangedEvent;

        event Action<float> SoundEffectVolumeChangedEvent; 

        event Action<string> LanguageChangedEvent;

        event Action<string[]> ContentPackChangedEvent;

        float BackgroundMusicVolume { get; set; }

        float BackgroundSoundVolume { get; set; }

        float MusicEffectVolume { get; set; }

        float SoundEffectVolume { get; set; }

        int ScreenWidth { get; }

        int ScreenHeight { get; }

        bool IsFullScreen { get; }

        string Language { get; set; }

        string[] ActiveContentPacks { get; set; }

        bool ShouldDisplayOfflineWarning { get; set; }

        bool IsDevelopmentMode { get; set; }

        void SetScreenSize(int screenWidth, int screenHeight, bool isFullScreen);

        void LoadSettings();

        void SaveSettings();
    }
}