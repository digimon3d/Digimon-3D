﻿using System;
using System.Globalization;
using UnityEngine;

namespace Digimon_3D.Service.Setting.Application.Model
{
    [Serializable]
    internal sealed class ApplicationSetting
    {
        [SerializeField]
        private float backgroundMusicVolume = 1.0f;

        [SerializeField]
        private float backgroundSoundVolume = 1.0f;

        [SerializeField]
        private float musicEffectVolume = 1.0f;

        [SerializeField]
        private float soundEffectVolume = 1.0f;

        [SerializeField]
        private int screenWidth = 1200;

        [SerializeField]
        private int screenHeight = 768;

        [SerializeField]
        private bool isFullScreen;

        [SerializeField]
        private string language = CultureInfo.CurrentUICulture.Name;

        [SerializeField]
        private string[] activeContentPacks = new string[0];

        [SerializeField]
        private bool shouldDisplayOfflineWarning = true;

        [SerializeField]
        private bool isDevelopmentMode;

        public float BackgroundMusicVolume
        {
            get => backgroundMusicVolume;
            set => backgroundMusicVolume = value;
        }

        public float BackgroundSoundVolume
        {
            get => backgroundSoundVolume;
            set => backgroundSoundVolume = value;
        }

        public float MusicEffectVolume
        {
            get => musicEffectVolume;
            set => musicEffectVolume = value;
        }

        public float SoundEffectVolume
        {
            get => soundEffectVolume;
            set => soundEffectVolume = value;
        }

        public int ScreenWidth
        {
            get => screenWidth;
            set => screenWidth = value;
        }

        public int ScreenHeight
        {
            get => screenHeight;
            set => screenHeight = value;
        }

        public bool IsFullScreen
        {
            get => isFullScreen;
            set => isFullScreen = value;
        }

        public string Language
        {
            get => language;
            set => language = value;
        }

        public string[] ActiveContentPacks
        {
            get => activeContentPacks;
            set => activeContentPacks = value;
        }

        public bool ShouldDisplayOfflineWarning
        {
            get => shouldDisplayOfflineWarning;
            set => shouldDisplayOfflineWarning = value;
        }

        public bool IsDevelopmentMode
        {
            get => isDevelopmentMode;
            set => isDevelopmentMode = value;
        }
    }
}