﻿using System;
using System.IO;
using Digimon_3D.Extensions.System;
using Digimon_3D.Service.Setting.Application.Model;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Service.Setting.Application
{
    internal sealed class ApplicationSettingService : IApplicationSettingService, IInitializable, IDisposable
    {
        public event Action<float> BackgroundMusicVolumeChangedEvent;

        public event Action<float> BackgroundSoundVolumeChangedEvent;

        public event Action<float> MusicEffectVolumeChangedEvent;

        public event Action<float> SoundEffectVolumeChangedEvent;

        public event Action<string> LanguageChangedEvent;

        public event Action<string[]> ContentPackChangedEvent;

        public float BackgroundMusicVolume
        {
            get => ApplicationSetting.BackgroundMusicVolume;
            set
            {
                ApplicationSetting.BackgroundMusicVolume = value;
                BackgroundMusicVolumeChangedEvent?.Invoke(value);
            }
        }

        public float BackgroundSoundVolume
        {
            get => ApplicationSetting.BackgroundSoundVolume;
            set
            {
                ApplicationSetting.BackgroundSoundVolume = value;
                BackgroundSoundVolumeChangedEvent?.Invoke(value);
            }
        }

        public float MusicEffectVolume
        {
            get => ApplicationSetting.MusicEffectVolume;
            set
            {
                ApplicationSetting.MusicEffectVolume = value;
                MusicEffectVolumeChangedEvent?.Invoke(value);
            }
        }

        public float SoundEffectVolume
        {
            get => ApplicationSetting.SoundEffectVolume;
            set
            {
                ApplicationSetting.SoundEffectVolume = value;
                SoundEffectVolumeChangedEvent?.Invoke(value);
            }
        }

        public int ScreenWidth => ApplicationSetting.ScreenWidth;

        public int ScreenHeight => ApplicationSetting.ScreenHeight;

        public bool IsFullScreen => ApplicationSetting.IsFullScreen;

        public string Language
        {
            get => ApplicationSetting.Language;
            set
            {
                ApplicationSetting.Language = value;
                LanguageChangedEvent?.Invoke(value);
            }
        }

        public string[] ActiveContentPacks
        {
            get => ApplicationSetting.ActiveContentPacks;
            set
            {
                ApplicationSetting.ActiveContentPacks = value;
                ContentPackChangedEvent?.Invoke(value);
            }
        }

        public bool ShouldDisplayOfflineWarning
        {
            get => ApplicationSetting.ShouldDisplayOfflineWarning;
            set => ApplicationSetting.ShouldDisplayOfflineWarning = value;
        }

        public bool IsDevelopmentMode
        {
            get => ApplicationSetting.IsDevelopmentMode;
            set => ApplicationSetting.IsDevelopmentMode = value;
        }

        private ApplicationSetting ApplicationSetting { get; set; }

        private string ApplicationSettingFilePath { get; }

        public ApplicationSettingService(string applicationSettingFilePath)
        {
            ApplicationSettingFilePath = applicationSettingFilePath;
        }

        public void Initialize()
        {
            ApplicationSetting = new ApplicationSetting();
            LoadSettings();
        }

        public void SetScreenSize(int screenWidth, int screenHeight, bool isFullScreen)
        {
            var minResolutionSize = Screen.resolutions[Screen.resolutions.GetLowerBound(0)];
            var maxResolutionSize = Screen.resolutions[Screen.resolutions.GetUpperBound(0)];

            ApplicationSetting.ScreenWidth = screenWidth.Clamp(minResolutionSize.width, maxResolutionSize.width);
            ApplicationSetting.ScreenHeight = screenHeight.Clamp(minResolutionSize.height, maxResolutionSize.height);
            ApplicationSetting.IsFullScreen = isFullScreen;

            Screen.SetResolution(ApplicationSetting.ScreenWidth, ApplicationSetting.ScreenHeight, isFullScreen);
        }

        public void LoadSettings()
        {
            if (!File.Exists(ApplicationSettingFilePath))
                return;

            using (var streamReader = new StreamReader(new FileStream(ApplicationSettingFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                JsonUtility.FromJsonOverwrite(streamReader.ReadToEnd(), ApplicationSetting);

            SetScreenSize(ApplicationSetting.ScreenWidth, ApplicationSetting.ScreenHeight, ApplicationSetting.IsFullScreen);
        }

        public void SaveSettings()
        {
            using (var streamWriter = new StreamWriter(new FileStream(ApplicationSettingFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)))
                streamWriter.Write(JsonUtility.ToJson(ApplicationSetting, true));
        }

        public void Dispose()
        {
            SaveSettings();
        }
    }
}