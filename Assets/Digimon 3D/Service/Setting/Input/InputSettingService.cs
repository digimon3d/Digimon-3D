﻿using System;
using System.IO;
using Digimon_3D.Service.Setting.Input.Model;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Service.Setting.Input
{
    internal sealed class InputSettingService : IInputSettingService, IInitializable, IDisposable
    {
        public Model.Input[] Inputs => InputSetting.Inputs;

        private InputSetting InputSetting { get; set; }

        private string InputSettingFilePath { get; }

        public InputSettingService(string inputSettingFilePath)
        {
            InputSettingFilePath = inputSettingFilePath;
        }

        public void Initialize()
        {
            InputSetting = new InputSetting();
            LoadSettings();
        }

        public void LoadSettings()
        {
            if (!File.Exists(InputSettingFilePath))
                return;

            using (var streamReader = new StreamReader(new FileStream(InputSettingFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                JsonUtility.FromJsonOverwrite(streamReader.ReadToEnd(), InputSetting);
        }

        public void SaveSettings()
        {
            using (var streamWriter = new StreamWriter(new FileStream(InputSettingFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)))
                streamWriter.Write(JsonUtility.ToJson(InputSetting, true));
        }

        public void Dispose()
        {
            SaveSettings();
        }
    }
}