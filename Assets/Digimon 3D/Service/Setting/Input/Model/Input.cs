﻿using System;
using UnityEngine;

namespace Digimon_3D.Service.Setting.Input.Model
{
    [Serializable]
    public sealed class Input
    {
        [SerializeField]
        private string name;

        [SerializeField]
        private InputCondition[] assignments = new InputCondition[0];

        public string Name
        {
            get => name;
            set => name = value;
        }

        public InputCondition[] Assignments
        {
            get => assignments;
            set => assignments = value;
        }
    }
}