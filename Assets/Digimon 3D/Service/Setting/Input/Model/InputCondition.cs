﻿using System;
using UnityEngine;

namespace Digimon_3D.Service.Setting.Input.Model
{
    [Serializable]
    public sealed class InputCondition
    {
        [SerializeField]
        private KeyCode[] conditions = new KeyCode[0];

        public KeyCode[] Conditions
        {
            get => conditions;
            set => conditions = value;
        }
    }
}