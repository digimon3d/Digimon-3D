﻿using System;
using UnityEngine;

namespace Digimon_3D.Service.Setting.Input.Model
{
    [Serializable]
    internal sealed class InputSetting
    {
        [SerializeField]
        private Input[] inputs =
        {
            new Input
            {
                Name = "ForwardMove",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.W } }
                }
            },
            new Input
            {
                Name = "LeftMove",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.A } }
                }
            },
            new Input
            {
                Name = "BackwardMove",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.S } }
                }
            },
            new Input
            {
                Name = "RightMove",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.D } }
                }
            },
            new Input
            {
                Name = "Up",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.UpArrow } }
                }
            },
            new Input
            {
                Name = "Left",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.LeftArrow } }
                }
            },
            new Input
            {
                Name = "Down",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.DownArrow } }
                }
            },
            new Input
            {
                Name = "Right",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.RightArrow } }
                }
            },
            new Input
            {
                Name = "Sprint",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.LeftShift } },
                    new InputCondition { Conditions = new[] { KeyCode.RightShift } }
                }
            },
            new Input
            {
                Name = "Enter",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.Return } },
                    new InputCondition { Conditions = new[] { KeyCode.KeypadEnter } }
                }
            },
            new Input
            {
                Name = "Back",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.E } }
                }
            },
            new Input
            {
                Name = "Escape",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.Escape } },
                    new InputCondition { Conditions = new[] { KeyCode.Pause } }
                }
            },
            new Input
            {
                Name = "Interact",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.Space } }
                }
            },
            new Input
            {
                Name = "Menu",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.E } }
                }
            },
            new Input
            {
                Name = "Gear",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.Q } }
                }
            },
            new Input
            {
                Name = "Mute",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.M } }
                }
            },
            new Input
            {
                Name = "Chat",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.T } }
                }
            },
            new Input
            {
                Name = "CamaraLock",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.C } }
                }
            },
            new Input
            {
                Name = "Screenshot",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.F2 } },
                    new InputCondition { Conditions = new[] { KeyCode.Print } }
                }
            },
            new Input
            {
                Name = "Debug",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.F3 } }
                }
            },
            new Input
            {
                Name = "PerspectiveSwitch",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.F5 } }
                }
            },
            new Input
            {
                Name = "FullScreen",
                Assignments = new[]
                {
                    new InputCondition { Conditions = new[] { KeyCode.F11 } },
                    new InputCondition { Conditions = new[] { KeyCode.LeftAlt, KeyCode.Return } },
                    new InputCondition { Conditions = new[] { KeyCode.RightAlt, KeyCode.Return } }
                }
            }
        };

        public Input[] Inputs
        {
            get => inputs;
            set => inputs = value;
        }
    }
}