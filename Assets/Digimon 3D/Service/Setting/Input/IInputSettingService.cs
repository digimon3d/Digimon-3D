﻿namespace Digimon_3D.Service.Setting.Input
{
    public interface IInputSettingService
    {
        Model.Input[] Inputs { get; }

        void LoadSettings();

        void SaveSettings();
    }
}
