﻿using System.IO;
using Digimon_3D.Scenes.MainMenu;
using Digimon_3D.Scenes.Overworld;
using Digimon_3D.Scenes.Splash;
using Digimon_3D.Service.Input;
using Digimon_3D.Service.Logger;
using Digimon_3D.Service.Setting.Application;
using Digimon_3D.Service.Setting.Input;
using Digimon_3D.Services.Audio;
using Digimon_3D.Services.Camera;
using Digimon_3D.Services.Content;
using Digimon_3D.Services.Content.Asset;
using Digimon_3D.Services.Content.ContentPack;
using Digimon_3D.Services.Debug;
using Digimon_3D.Services.GameMode;
using Digimon_3D.Services.GameMode.Data;
using Digimon_3D.Services.Scene;
using Digimon_3D.Services.Video;
using Digimon_3D.Services.World;
using Digimon_3D.Views.Debug;
using Digimon_3D.Views.MainMenu;
using Digimon_3D.Views.Splash;
using Digimon_3D.Views.World;
using UnityEngine;
using Zenject;

namespace Digimon_3D
{
    internal sealed class Game : MonoInstaller<Game>
    {
        public const string Name = "Digimon 3D";

        public const string Version = "0.0.1";

        public const string Developer = "-N- Tertain / The Dialga Team";

        public const bool IsDebugMode = true;

        private string AppDataDirectory { get; set; }

        private string StreamingAssetsDirectory { get; set; }

        public override void InstallBindings()
        {
            AppDataDirectory = Path.GetFullPath(Application.persistentDataPath);

#if UNITY_EDITOR
            StreamingAssetsDirectory = Path.GetFullPath(Path.Combine(Application.dataPath, "..", "Bin", "Debug", "Digimon 3D_Data", "StreamingAssets"));

            if (!Directory.Exists(StreamingAssetsDirectory))
                throw new FileNotFoundException("Unable to find debug assets. Ensure you have built debug assets via Digimon 3D > Build > Debug.");
#else
            StreamingAssetsDirectory = Path.GetFullPath(UnityEngine.Application.streamingAssetsPath);
#endif

            InstallService();
        }

        private void InstallService()
        {
            Container.BindInterfacesTo<LoggerService>().AsSingle().WithArguments(Path.Combine(StreamingAssetsDirectory, "Logger.log"));

            Container.BindInterfacesTo<ApplicationSettingService>().AsSingle().WithArguments(Path.Combine(StreamingAssetsDirectory, "ApplicationSetting.json"));
            Container.BindInterfacesTo<InputSettingService>().AsSingle().WithArguments(Path.Combine(StreamingAssetsDirectory, "InputSetting.json"));

            Container.BindInterfacesTo<InputService>().AsSingle();
            Container.Bind<CustomInputModule>().FromComponentInNewPrefabResource("Digimon 3D/Service/Input/InputEvent").WithGameObjectName("InputEvent").AsSingle().NonLazy();
        }

        private void InstallServices()
        {
            // Digimon_3D.Services.GameMode
            Container.BindInterfacesAndSelfTo<GameModeService>().AsSingle();

            // Digimon_3D.Services.GameMode.Data
            Container.BindInterfacesAndSelfTo<GameModeDataService>().AsSingle();

            // Digimon_3D.Services.Content.ContentPack
            Container.BindInterfacesAndSelfTo<ContentPackService>().AsSingle();

            // Digimon_3D.Services.Content.Asset
            Container.BindFactory<GameAssetsCollectionConfig, GameAssetsCollection, GameAssetsCollection.Factory>();

            // Digimon_3D.Services.Content
            Container.BindInterfacesAndSelfTo<ContentService>().AsSingle();

            // Digimon_3D.Services.Audio
            Container.BindInterfacesAndSelfTo<MusicPlayerService>().FromComponentInNewPrefabResource("Digimon 3D/Service/Audio/MusicPlayerService").WithGameObjectName(nameof(MusicPlayerService)).AsSingle();

            // Digimon_3D.Services.Video
            Container.BindInterfacesAndSelfTo<VideoPlayerService>().FromComponentInNewPrefabResource("Digimon 3D/Service/Video/VideoPlayerService").WithGameObjectName(nameof(VideoPlayerService)).AsSingle().NonLazy();

            // Digimon_3D.Services.Debug
            Container.BindInterfacesAndSelfTo<DebugService>().AsSingle();

            // Digimon_3D.Services.World
            Container.BindInterfacesAndSelfTo<WorldService>().AsSingle();

            // Digimon_3D.Services.Camera
            Container.BindInterfacesAndSelfTo<CameraService>().FromComponentInNewPrefabResource("Digimon 3D/View/Camera/CameraComponent").WithGameObjectName(nameof(CameraService)).AsSingle().NonLazy();

            // Digimon_3D.Services.Scene
            Container.BindInterfacesAndSelfTo<SceneService>().AsSingle().NonLazy();
        }

        private void InstallScenes()
        {
            Container.BindFactory<SplashScene, SplashScene.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(SplashScene));
            Container.BindFactory<MainMenuScene, MainMenuScene.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(MainMenuScene));
            Container.BindFactory<OverworldScene, OverworldScene.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(OverworldScene));
        }

        private void InstallViews()
        {
            Container.BindFactory<DebugView, DebugView.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(DebugView));
            Container.BindFactory<DebugView, DebugComponent, DebugComponent.Factory>().FromComponentInNewPrefabResource("Digimon 3D/View/Debug/DebugComponent").WithGameObjectName(nameof(DebugComponent));

            Container.BindFactory<MainMenuView, MainMenuView.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(MainMenuView));
            Container.BindFactory<MainMenuView, LoginScreenComponent, LoginScreenComponent.Factory>().FromComponentInNewPrefabResource("Digimon 3D/View/MainMenu/LoginScreenComponent").WithGameObjectName(nameof(LoginScreenComponent));

            Container.BindFactory<SplashView, SplashView.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(SplashView));
            Container.BindFactory<SplashView, SplashComponent, SplashComponent.Factory>().FromComponentInNewPrefabResource("Digimon 3D/View/Splash/SplashComponent").WithGameObjectName(nameof(SplashComponent));

            Container.BindFactory<WorldView, WorldView.Factory>().FromNewComponentOnNewGameObject().WithGameObjectName(nameof(WorldView));
            Container.BindFactory<WorldView, WorldComponent, WorldComponent.Factory>().FromComponentInNewPrefabResource("Digimon 3D/View/World/WorldComponent").WithGameObjectName(nameof(WorldComponent));
        }
    }

    internal sealed class Start : IInitializable
    {
        private ISceneService SceneService { get; }

        public Start(ISceneService sceneService)
        {
            SceneService = sceneService;
        }

        public void Initialize()
        {
            if (Game.IsDebugMode || Application.isEditor)
                SceneService.ChangeScene<SplashScene>();
            else
                SceneService.ChangeScene<SplashScene>();
        }
    }
}