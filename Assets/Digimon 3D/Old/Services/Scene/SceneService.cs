﻿using System;
using Digimon_3D.Scenes;
using Digimon_3D.Scenes.MainMenu;
using Digimon_3D.Scenes.Overworld;
using Digimon_3D.Scenes.Splash;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Digimon_3D.Services.Scene
{
    internal interface ISceneService
    {
        IScene ChangeScene<TScene>() where TScene : MonoBehaviour, IScene;
    }

    [UsedImplicitly]
    internal sealed class SceneService : ISceneService, IDisposable
    {
        [Inject]
        private SplashScene.Factory SplashSceneFactory { get; }

        [Inject]
        private MainMenuScene.Factory MainMenuSceneFactory { get; }

        [Inject]
        private OverworldScene.Factory OverworldSceneFactory { get; }

        private IScene CurrentScene { get; set; }

        public IScene ChangeScene<TScene>() where TScene : MonoBehaviour, IScene
        {
            if (typeof(TScene) == typeof(SplashScene))
                return GenerateNewScene(SplashSceneFactory);

            if (typeof(TScene) == typeof(MainMenuScene))
                return GenerateNewScene(MainMenuSceneFactory);

            if (typeof(TScene) == typeof(OverworldScene))
                return GenerateNewScene(OverworldSceneFactory);

            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Object.Destroy(CurrentScene.SceneGameObject);
        }

        private IScene GenerateNewScene<TScene>(IFactory<TScene> sceneFactory) where TScene : MonoBehaviour, IScene
        {
            var newScene = sceneFactory.Create();

            if (!newScene)
                return null;

            if (CurrentScene != null)
                Object.Destroy(CurrentScene.SceneGameObject);

            CurrentScene = newScene;

            return newScene;
        }
    }
}