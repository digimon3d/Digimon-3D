﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Digimon_3D.Service.Input;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.Debug
{
    internal interface IDebuggable
    {
        string GetDebugString();
    }

    [UsedImplicitly]
    internal sealed class DebugService : IInitializable, ITickable, ILateTickable
    {
        public bool CanShowDebugMode { get; set; }

        public bool CanShowDeveloper { get; set; }

        public DebugModes DebugMode { get; set; } = DebugModes.None;

        public float CurrentFPS { get; set; }

        private IInputService InputService { get; }

        private Stopwatch Stopwatch { get; } = new Stopwatch();

        private int CurrentFrame { get; set; }

        private List<IDebuggable> DebugObjects { get; } = new List<IDebuggable>();

        private StringBuilder StringBuilder { get; } = new StringBuilder();

        public DebugService(IInputService inputService)
        {
            InputService = inputService;
        }

        public void Initialize()
        {
            Stopwatch.Start();
        }

        public void Tick()
        {
            if (!InputService.IsKeyDown("Debug"))
                return;

            switch (DebugMode)
            {
                case DebugModes.None:
                    DebugMode = DebugModes.Simple;
                    break;

                case DebugModes.Simple:
                    DebugMode = DebugModes.Detailed;
                    break;

                case DebugModes.Detailed:
                    DebugMode = DebugModes.None;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void LateTick()
        {
            CurrentFrame++;

            if (Stopwatch.ElapsedMilliseconds < 1000)
                return;

            Stopwatch.Stop();

            CurrentFPS = CurrentFrame / (Stopwatch.ElapsedMilliseconds / 1000.0f);
            CurrentFrame = 0;

            Stopwatch.Restart();
        }

        public void RegisterDebugObject(IDebuggable obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            DebugObjects.Add(obj);
        }

        public void UnregisterDebugObject(IDebuggable obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            DebugObjects.Remove(obj);
        }

        public string GetDebugString()
        {
            StringBuilder.Clear();

            foreach (var obj in DebugObjects)
                StringBuilder.AppendLine(obj.GetDebugString());

            return StringBuilder.ToString();
        }
    }
}