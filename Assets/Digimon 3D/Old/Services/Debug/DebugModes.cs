﻿namespace Digimon_3D.Services.Debug
{
    public enum DebugModes
    {
        None = 0,

        Simple = 1,

        Detailed = 2
    }
}