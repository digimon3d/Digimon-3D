﻿using System;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Service.Setting.Application;
using Digimon_3D.Services.Content;
using Digimon_3D.Services.Content.Asset;
using Digimon_3D.Services.IO;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;

namespace Digimon_3D.Services.Video
{
    internal sealed class VideoPlayerService : MonoBehaviour, IInitializable, ITickable, IDisposable
    {
        [SerializeField]
        private VideoPlayer _videoPlayer;

        [SerializeField]
        private AudioSource _audioSource;

        [SerializeField]
        private GameObject _rawImageObject;

        [SerializeField]
        private RawImage _rawImage;

        public VideoStatus Status { get; private set; } = VideoStatus.Stopped;

        [Inject]
        private DirectoryService DirectoryService { get; }

        [Inject]
        private ApplicationSettingService ApplicationSettingsService { get; }

        [Inject]
        private ContentService ContentService { get; }

        private VideoAsset VideoAsset { get; set; }

        private bool IsGameModeAssets { get; set; }

        public void Initialize()
        {
            _videoPlayer.skipOnDrop = false;
            _videoPlayer.sendFrameReadyEvents = true;

            _audioSource.ignoreListenerPause = true;
            _audioSource.ignoreListenerVolume = true;
            _audioSource.volume = ApplicationSettingsService.BackgroundMusicVolume;

            ApplicationSettingsService.BackgroundMusicVolumeChangedEvent += OnMusicVolumeChanged;
        }

        public void Tick()
        {
            if ((Status == VideoStatus.Playing || Status == VideoStatus.Paused || Status == VideoStatus.Resumed) && !_rawImageObject.activeInHierarchy)
                _rawImageObject.SetActive(true);

            if (Status == VideoStatus.Stopped && _rawImageObject.activeInHierarchy)
                _rawImageObject.SetActive(false);

            if ((Status == VideoStatus.Playing || Status == VideoStatus.Resumed) && !_videoPlayer.isPlaying)
                Stop();
        }

        public void Dispose()
        {
            ApplicationSettingsService.BackgroundMusicVolumeChangedEvent -= OnMusicVolumeChanged;
        }

        public async UniTask PlayAsync(string assetPointer, bool isGameModeAssets = false)
        {
            if (assetPointer == null)
                throw new ArgumentNullException(nameof(assetPointer));

            if (IsGameModeAssets == isGameModeAssets && VideoAsset != null)
            {
                foreach (var videoAssetPointer in VideoAsset.AssetPointers)
                {
                    if (string.Format(DirectoryService.VideoContentDirectoryPointerFormat, assetPointer).ResolveFilePath().Equals(videoAssetPointer, StringComparison.OrdinalIgnoreCase))
                        return;
                }
            }

            Stop();

            var videoAsset = await ContentService.GetVideoAssetAsync(assetPointer, isGameModeAssets);

            if (videoAsset == null)
                return;

            _videoPlayer.source = VideoSource.Url;
            _videoPlayer.url = $"{videoAsset.VideoFile}";
            _videoPlayer.EnableAudioTrack(0, true);
            _videoPlayer.SetTargetAudioSource(0, _audioSource);
            _videoPlayer.Prepare();

            Status = VideoStatus.Preparing;

            await UniTask.WaitWhile(() => !_videoPlayer.isPrepared);

            Status = VideoStatus.Playing;

            _rawImage.texture = _videoPlayer.texture;

            _videoPlayer.Play();
            _audioSource.Play();

            VideoAsset = videoAsset;
            IsGameModeAssets = isGameModeAssets;
        }

        public void Pause()
        {
            Status = VideoStatus.Paused;
            _videoPlayer.Pause();
        }

        public void Stop()
        {
            Status = VideoStatus.Stopped;
            _videoPlayer.Stop();
        }

        public void Resume()
        {
            Status = VideoStatus.Resumed;
            _videoPlayer.Play();
        }

        private void OnMusicVolumeChanged(float volume)
        {
            _audioSource.volume = volume;
        }
    }
}