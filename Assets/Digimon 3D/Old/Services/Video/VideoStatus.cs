﻿namespace Digimon_3D.Services.Video
{
    internal enum VideoStatus
    {
        Preparing,

        Playing,

        Paused,

        Stopped,

        Resumed
    }
}