﻿using System;
using System.IO;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Services.GameMode.Data
{
    [UsedImplicitly]
    internal sealed class GameModeDataService : IInitializable, IDisposable
    {
        public EntityDefinition.EntityDefinition EntityDefinition { get; private set; }

        private FilePathService FilePathService { get; }

        private GameModeService GameModeService { get; }

        public GameModeDataService(FilePathService filePathService, GameModeService gameModeService)
        {
            FilePathService = filePathService;
            GameModeService = gameModeService;
        }

        public void Initialize()
        {
            EntityDefinition = new EntityDefinition.EntityDefinition();
            GameModeService.GameModeChangedEvent += GameModeServiceOnGameModeChangedEvent;
        }

        public void Dispose()
        {
            GameModeService.GameModeChangedEvent -= GameModeServiceOnGameModeChangedEvent;
        }

        private void GameModeServiceOnGameModeChangedEvent(GameMode gameMode)
        {
            var entityDefinitionFilePath = string.Format(FilePathService.GameModeDataEntityDefinitionFilePathFormat, $"{gameMode.GameModeBasePath}/{gameMode.GameModeInfo.DataPath}").ResolveFullPath();

            using (var reader = new StreamReader(new FileStream(entityDefinitionFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                JsonUtility.FromJsonOverwrite(reader.ReadToEnd(), EntityDefinition);
        }
    }
}