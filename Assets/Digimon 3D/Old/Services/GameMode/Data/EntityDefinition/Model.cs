﻿using System;

namespace Digimon_3D.Services.GameMode.Data.EntityDefinition
{
    [Serializable]
    internal sealed class Model
    {
        public string Name;

        public Mesh[] Meshes = new Mesh[0];
    }
}