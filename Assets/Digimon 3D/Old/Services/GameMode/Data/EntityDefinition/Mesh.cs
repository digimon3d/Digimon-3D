﻿using System;
using UnityEngine;

namespace Digimon_3D.Services.GameMode.Data.EntityDefinition
{
    [Serializable]
    internal sealed class Mesh
    {
        public Vector3[] Vertices = new Vector3[0];

        public int[] Triangles = new int[0];

        public Vector3[] Normals = new Vector3[0];

        public Vector2[] UVs = new Vector2[0];
    }
}