﻿using System;

namespace Digimon_3D.Services.GameMode.Data.EntityDefinition
{
    [Serializable]
    internal sealed class EntityDefinition
    {
        public Model[] Models = new Model[0];
    }
}