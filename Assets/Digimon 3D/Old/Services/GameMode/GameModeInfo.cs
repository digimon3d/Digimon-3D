﻿using System;

namespace Digimon_3D.Services.GameMode
{
    [Serializable]
    internal sealed class GameModeInfo
    {
        public string Author = string.Empty;

        public string Description = string.Empty;

        public string Version = string.Empty;

        public string ContentPath = "Content";

        public string DataPath = "Data";

        public string MapPath = "Map";

        public string ScriptPath = "Script";
    }
}