﻿using System;
using System.Collections.Generic;
using System.IO;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.GameMode
{
    [UsedImplicitly]
    internal sealed class GameModeService : IInitializable
    {
        public event Action<GameMode> GameModeChangedEvent;

        public List<GameMode> GameModes { get; private set; }

        public GameMode ActiveGameMode { get; private set; }

        private DirectoryService DirectoryService { get; }

        private FilePathService FilePathService { get; }

        public GameModeService(DirectoryService directoryService, FilePathService filePathService)
        {
            DirectoryService = directoryService;
            FilePathService = filePathService;
        }

        public void Initialize()
        {
            GameModes = new List<GameMode>();
            LoadGameModeDefinition();
        }

        public void LoadGameModeDefinition()
        {
            var gameModeDirectories = Directory.GetDirectories(DirectoryService.GameModeDirectory, "*", SearchOption.TopDirectoryOnly);

            foreach (var gameModeDirectory in gameModeDirectories)
            {
                var gameModeInfoFilePath = string.Format(FilePathService.GameModeFilePathFormat, gameModeDirectory).ResolveFullPath();

                if (!File.Exists(gameModeInfoFilePath))
                    continue;

                using (var reader = new StreamReader(new FileStream(gameModeInfoFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                    GameModes.Add(new GameMode(gameModeDirectory, reader.ReadToEnd()));
            }
        }

        public void SetActiveGameMode(GameMode gameMode)
        {
            ActiveGameMode = gameMode;
            GameModeChangedEvent?.Invoke(gameMode);
        }
    }
}