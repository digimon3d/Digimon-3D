﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Services.Camera
{
    [UsedImplicitly]
    internal sealed class CameraService : MonoBehaviour, IInitializable
    {
        public void Initialize()
        {
            transform.localPosition = Vector3.zero;
        }
    }
}