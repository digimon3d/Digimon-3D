﻿using System;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Service.Setting.Application;
using Digimon_3D.Services.Content;
using Digimon_3D.Services.Content.Asset;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using UniRx.Async;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Services.Audio
{
    [UsedImplicitly]
    internal sealed class MusicPlayerService : MonoBehaviour, IInitializable, ITickable, IDisposable
    {
        [SerializeField]
        private AudioSource _audioSource;

        [Inject]
        private DirectoryService DirectoryService { get; }

        [Inject]
        private ApplicationSettingService ApplicationSettingsService { get; }

        [Inject]
        private ContentService ContentService { get; }

        private MusicAsset MusicAsset { get; set; }

        private bool IsGameModeAssets { get; set; }

        private MusicStatus MusicStatus { get; set; } = MusicStatus.Stopped;

        public void Initialize()
        {
            _audioSource.ignoreListenerPause = true;
            _audioSource.ignoreListenerVolume = true;
            _audioSource.volume = ApplicationSettingsService.BackgroundMusicVolume;

            ApplicationSettingsService.BackgroundMusicVolumeChangedEvent += ApplicationSettingsServiceOnMusicVolumeChangedEvent;
        }

        public void Tick()
        {
            if ((MusicStatus == MusicStatus.PlayingIntro || MusicStatus == MusicStatus.Resumed) && !_audioSource.isPlaying)
                PlayAsync(MusicAsset.AssetPointers[0], IsGameModeAssets, false).Forget();

            if (MusicStatus != MusicStatus.PlayingMain && MusicStatus != MusicStatus.Resumed)
                return;

            if (MusicAsset.AssetModifier.EndPCMSample <= 0)
                return;

            if (_audioSource.timeSamples >= MusicAsset.AssetModifier.EndPCMSample)
                _audioSource.timeSamples = MusicAsset.AssetModifier.LoopPCMSample;
        }

        public void Dispose()
        {
            ApplicationSettingsService.BackgroundMusicVolumeChangedEvent -= ApplicationSettingsServiceOnMusicVolumeChangedEvent;
        }

        public async UniTask PlayAsync(string assetPointer, bool isGameModeAssets = false, bool playIntro = true)
        {
            if (assetPointer == null)
                throw new ArgumentNullException(nameof(assetPointer));

            if (IsGameModeAssets == isGameModeAssets && MusicAsset != null)
            {
                foreach (var musicAssetPointer in MusicAsset.AssetPointers)
                {
                    if (string.Format(DirectoryService.MusicContentDirectoryPointerFormat, assetPointer).ResolveFilePath().Equals(musicAssetPointer, StringComparison.OrdinalIgnoreCase))
                        return;
                }
            }

            Stop();

            var musicAsset = await ContentService.GetMusicAssetAsync(assetPointer, isGameModeAssets);

            if (musicAsset == null)
                return;

            if (musicAsset.MusicIntro && playIntro)
            {
                MusicStatus = MusicStatus.PlayingIntro;
                _audioSource.clip = musicAsset.MusicIntro;
                _audioSource.loop = false;
                _audioSource.Play();
            }
            else if (musicAsset.MusicLoop)
            {
                MusicStatus = MusicStatus.PlayingMain;
                _audioSource.clip = musicAsset.MusicLoop;
                _audioSource.loop = true;
                _audioSource.timeSamples = musicAsset.AssetModifier.StartPCMSample;
                _audioSource.Play();
            }

            MusicAsset = musicAsset;
            IsGameModeAssets = isGameModeAssets;
        }

        public void Pause()
        {
            MusicStatus = MusicStatus.Paused;
            _audioSource.Pause();
        }

        public void Stop()
        {
            MusicStatus = MusicStatus.Stopped;
            _audioSource.Stop();
        }

        public void Resume()
        {
            MusicStatus = MusicStatus.Resumed;
            _audioSource.UnPause();
        }

        private void ApplicationSettingsServiceOnMusicVolumeChangedEvent(float volume)
        {
            _audioSource.volume = volume;
        }
    }
}