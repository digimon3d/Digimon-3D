﻿namespace Digimon_3D.Services.Audio
{
    public enum MusicStatus
    {
        PlayingIntro,

        PlayingMain,

        Paused,

        Stopped,

        Resumed
    }
}