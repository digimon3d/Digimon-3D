﻿using System;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Services.Content.Asset;
using Digimon_3D.Services.GameMode;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using UniRx.Async;
using Zenject;

namespace Digimon_3D.Services.Content
{
    [UsedImplicitly]
    internal sealed class ContentService : IInitializable, IDisposable
    {
        private DirectoryService DirectoryService { get; }

        private FilePathService FilePathService { get; }

        private GameModeService GameModeService { get; }

        private GameAssetsCollection.Factory GameAssetsCollectionFactory { get; }

        private GameAssetsCollection GameAssetsCollection { get; set; }

        private GameAssetsCollection GameModeAssetsCollection { get; set; }

        public ContentService(DirectoryService directoryService, FilePathService filePathService, GameModeService gameModeService, GameAssetsCollection.Factory gameAssetsCollectionFactory)
        {
            DirectoryService = directoryService;
            FilePathService = filePathService;
            GameModeService = gameModeService;
            GameAssetsCollectionFactory = gameAssetsCollectionFactory;
        }

        public void Initialize()
        {
            GameAssetsCollection = GameAssetsCollectionFactory.Create(new GameAssetsCollectionConfig(DirectoryService.ContentDirectory, string.Format(FilePathService.AssetModifierFilePathFormat, DirectoryService.ContentDirectory)));
            GameModeService.GameModeChangedEvent += GameModeServiceOnGameModeChangedEvent;
        }

        public void Dispose()
        {
            GameModeService.GameModeChangedEvent -= GameModeServiceOnGameModeChangedEvent;
        }

        public async UniTask<MusicAsset> GetMusicAssetAsync(string assetPointer, bool isGameModeAssets)
        {
            return await GetAssetAsync<MusicAsset>(string.Format(DirectoryService.MusicContentDirectoryPointerFormat, assetPointer).ResolveFilePath(), isGameModeAssets);
        }

        public async UniTask<SoundAsset> GetSoundAssetAsync(string assetPointer, bool isGameModeAssets)
        {
            return await GetAssetAsync<SoundAsset>(string.Format(DirectoryService.SoundContentDirectoryPointerFormat, assetPointer).ResolveFilePath(), isGameModeAssets);
        }

        public async UniTask<TextureAsset> GetTextureAssetAsync(string assetPointer, bool isGameModeAssets)
        {
            return await GetAssetAsync<TextureAsset>(string.Format(DirectoryService.TextureContentDirectoryPointerFormat, assetPointer).ResolveFilePath(), isGameModeAssets);
        }

        public async UniTask<VideoAsset> GetVideoAssetAsync(string assetPointer, bool isGameModeAssets)
        {
            return await GetAssetAsync<VideoAsset>(string.Format(DirectoryService.VideoContentDirectoryPointerFormat, assetPointer).ResolveFilePath(), isGameModeAssets);
        }

        private void GameModeServiceOnGameModeChangedEvent(GameMode.GameMode gameMode)
        {
            if (GameModeAssetsCollection != null)
            {
                foreach (var gameAsset in GameModeAssetsCollection.Assets)
                    gameAsset.Dispose();
            }

            var gameModeContentPath = $"{gameMode.GameModeBasePath}/{gameMode.GameModeInfo.ContentPath}".ResolveFullPath();
            var gameModeContentModifierFilePath = string.Format(FilePathService.AssetModifierFilePathFormat, gameModeContentPath).ResolveFullPath();

            GameModeAssetsCollection = new GameAssetsCollection(new GameAssetsCollectionConfig(gameModeContentPath, gameModeContentModifierFilePath), DirectoryService);
        }

        private async UniTask<TAssetType> GetAssetAsync<TAssetType>(string assetPointer, bool isGameModeAssets) where TAssetType : class, IGameAsset
        {
            IGameAsset resolvedAsset = null;

            if (!isGameModeAssets)
            {
                foreach (var gameAsset in GameAssetsCollection.Assets)
                {
                    foreach (var gameAssetAssetPointer in gameAsset.AssetPointers)
                    {
                        if (gameAssetAssetPointer.Equals(assetPointer, StringComparison.OrdinalIgnoreCase))
                            resolvedAsset = gameAsset is TAssetType asset ? asset : null;
                    }
                }
            }
            else
            {
                foreach (var gameAsset in GameModeAssetsCollection.Assets)
                {
                    foreach (var gameAssetAssetPointer in gameAsset.AssetPointers)
                    {
                        if (gameAssetAssetPointer.Equals(assetPointer, StringComparison.OrdinalIgnoreCase))
                            resolvedAsset = gameAsset is TAssetType asset ? asset : null;
                    }
                }
            }

            if (resolvedAsset == null)
                return null;

            await resolvedAsset.PrepareAsync();

            return (TAssetType) resolvedAsset;
        }
    }
}