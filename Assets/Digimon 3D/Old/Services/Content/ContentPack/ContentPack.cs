﻿using System.IO;
using UnityEngine;

namespace Digimon_3D.Services.Content.ContentPack
{
    internal sealed class ContentPack
    {
        public string ContentId { get; }

        public string ContentBasePath { get; }

        public ContentPackInfo ContentPackInfo { get; }

        public ContentPack(string contentBasePath, string json)
        {
            ContentId = Path.GetDirectoryName(contentBasePath);
            ContentBasePath = contentBasePath;
            ContentPackInfo = JsonUtility.FromJson<ContentPackInfo>(json);
        }
    }
}