﻿using System;
using System.Collections.Generic;
using System.IO;
using Digimon_3D.Service.Setting.Application;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.Content.ContentPack
{
    [UsedImplicitly]
    internal sealed class ContentPackService : IInitializable, IDisposable
    {
        public ContentPack[] ActiveContentPacks { get; private set; } = new ContentPack[0];

        private DirectoryService DirectoryService { get; }

        private FilePathService FilePathService { get; }

        private ApplicationSettingService ApplicationSettingsService { get; }

        public ContentPackService(DirectoryService directoryService, FilePathService filePathService, ApplicationSettingService applicationSettingsService)
        {
            DirectoryService = directoryService;
            FilePathService = filePathService;
            ApplicationSettingsService = applicationSettingsService;
        }

        public void Initialize()
        {
            RefreshContentPackPointer(ApplicationSettingsService.ActiveContentPacks);
            ApplicationSettingsService.ContentPackChangedEvent += RefreshContentPackPointer;
        }

        public void Dispose()
        {
            ApplicationSettingsService.ContentPackChangedEvent -= RefreshContentPackPointer;
        }

        public ContentPack[] GetContentPackList()
        {
            if (!Directory.Exists(DirectoryService.ContentPackDirectory))
                return new ContentPack[0];

            var contentPacks = new List<ContentPack>();
            var contentPackDirectories = Directory.GetDirectories(DirectoryService.ContentPackDirectory, "*", SearchOption.TopDirectoryOnly);

            foreach (var contentPackDirectory in contentPackDirectories)
            {
                var contentPackFilePath = string.Format(FilePathService.ContentPackFilePathFormat, contentPackDirectory);

                if (!File.Exists(contentPackFilePath))
                    continue;

                using (var reader = new StreamReader(new FileStream(contentPackFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                    contentPacks.Add(new ContentPack(contentPackDirectory, reader.ReadToEnd()));
            }

            return contentPacks.ToArray();
        }

        private void RefreshContentPackPointer(string[] activeContentPacks)
        {
            var contentPackActive = new List<ContentPack>();
            var contentPackList = GetContentPackList();

            foreach (var activeContentPack in activeContentPacks)
            {
                foreach (var contentPack in contentPackList)
                {
                    if (!contentPack.ContentId.Equals(activeContentPack, StringComparison.OrdinalIgnoreCase))
                        continue;

                    contentPackActive.Add(contentPack);
                    break;
                }
            }

            ActiveContentPacks = contentPackActive.ToArray();
        }
    }
}