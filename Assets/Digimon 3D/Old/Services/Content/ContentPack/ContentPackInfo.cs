﻿using System;

namespace Digimon_3D.Services.Content.ContentPack
{
    [Serializable]
    internal sealed class ContentPackInfo
    {
        public string Author;

        public string Description;

        public string Version;
    }
}