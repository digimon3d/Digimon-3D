﻿using System;

namespace Digimon_3D.Services.Content.Asset.Modifier
{
    [Serializable]
    internal class AssetModifier
    {
        public MusicModifier[] MusicModifiers = new MusicModifier[0];

        public SoundModifier[] SoundModifiers = new SoundModifier[0];

        public TextureModifier[] TextureModifiers = new TextureModifier[0];

        public BaseModifier[] VideoModifiers = new BaseModifier[0];
    }
}