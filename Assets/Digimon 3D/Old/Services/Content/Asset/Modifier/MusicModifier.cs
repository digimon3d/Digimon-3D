﻿using System;

namespace Digimon_3D.Services.Content.Asset.Modifier
{
    [Serializable]
    internal sealed class MusicModifier : SoundModifier
    {
        public int LoopPCMSample;
    }
}