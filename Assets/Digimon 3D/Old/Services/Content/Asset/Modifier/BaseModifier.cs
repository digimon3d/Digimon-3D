﻿using System;

namespace Digimon_3D.Services.Content.Asset.Modifier
{
    [Serializable]
    internal class BaseModifier
    {
        public string AssetPointer = string.Empty;

        public string[] AssetAliasPointers = new string[0];
    }
}