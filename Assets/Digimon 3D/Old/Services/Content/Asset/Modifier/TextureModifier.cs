﻿using System;

namespace Digimon_3D.Services.Content.Asset.Modifier
{
    [Serializable]
    internal sealed class TextureModifier : BaseModifier
    {
        public float SizeMultiplier = 1;
    }
}