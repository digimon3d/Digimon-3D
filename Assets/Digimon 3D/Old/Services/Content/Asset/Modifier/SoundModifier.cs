﻿using System;

namespace Digimon_3D.Services.Content.Asset.Modifier
{
    [Serializable]
    internal class SoundModifier : BaseModifier
    {
        public int StartPCMSample;

        public int EndPCMSample = -1;
    }
}