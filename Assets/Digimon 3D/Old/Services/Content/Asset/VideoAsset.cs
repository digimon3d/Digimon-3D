﻿using System.IO;
using Digimon_3D.Extensions.System.Security.Cryptography;
using Digimon_3D.Services.Content.Asset.Modifier;
using UniRx.Async;

namespace Digimon_3D.Services.Content.Asset
{
    internal sealed class VideoAsset : GameAsset<string, BaseModifier>
    {
        public string VideoFile => Asset[0];

        public VideoAsset(string[] assetFilePath, BaseModifier assetModifier) : base(assetFilePath, assetModifier)
        {
        }

        public override UniTask PrepareAsync()
        {
            if (!File.Exists(AssetFilePath[0]))
                throw new FileNotFoundException("The requested file does not exist.", Path.GetFileName(AssetFilePath[0]));

            var fileHash = new FileStream(AssetFilePath[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite).ToMD5();

            if (AssetFileHash[0].Equals(fileHash))
                return UniTask.CompletedTask;

            Asset[0] = AssetFilePath[0];
            AssetFileHash[0] = fileHash;

            return UniTask.CompletedTask;
        }

        protected override void ReleaseUnmanagedResources()
        {
        }
    }
}