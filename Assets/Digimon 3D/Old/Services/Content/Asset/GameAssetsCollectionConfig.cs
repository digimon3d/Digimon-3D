﻿namespace Digimon_3D.Services.Content.Asset
{
    internal struct GameAssetsCollectionConfig
    {
        public string ContentFolder { get; }

        public string AssetModifierFile { get; }

        public GameAssetsCollectionConfig(string contentFolder, string assetModifierFile)
        {
            ContentFolder = contentFolder;
            AssetModifierFile = assetModifierFile;
        }
    }
}