﻿using System.Collections.Generic;
using System.IO;
using Digimon_3D.Extensions.System.Security.Cryptography;
using Digimon_3D.Services.Content.Asset.Modifier;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;

namespace Digimon_3D.Services.Content.Asset
{
    internal class TextureAsset : GameAsset<Texture2D, TextureModifier>
    {
        public Texture2D Texture => Asset[0];
        private Dictionary<RectInt, Texture2D> RectTextures { get; } = new Dictionary<RectInt, Texture2D>();

        public TextureAsset(string[] assetFilePath, TextureModifier assetModifier) : base(assetFilePath, assetModifier)
        {
        }

        public Texture2D GetTexture(RectInt rectangle)
        {
            var modifierRectangle = rectangle;
            modifierRectangle.width = (int) (modifierRectangle.width * AssetModifier.SizeMultiplier);
            modifierRectangle.height = (int) (modifierRectangle.height * AssetModifier.SizeMultiplier);

            if (RectTextures.TryGetValue(modifierRectangle, out var texture))
                return texture;

            var newTexture = new Texture2D(modifierRectangle.width, modifierRectangle.height) { name = $"{Texture.name}/{modifierRectangle}" };
            newTexture.SetPixels(Texture.GetPixels(modifierRectangle.x, Texture.height - modifierRectangle.y - modifierRectangle.height, modifierRectangle.width, modifierRectangle.height));
            newTexture.filterMode = FilterMode.Point;
            newTexture.wrapMode = TextureWrapMode.Clamp;
            newTexture.Apply();

            RectTextures.Add(modifierRectangle, newTexture);

            return newTexture;
        }

        public Texture2D[] GetTextures(RectInt[] rectangles)
        {
            var textures = new List<Texture2D>();

            foreach (var rectangle in rectangles)
                textures.Add(GetTexture(rectangle));

            return textures.ToArray();
        }

        public override async UniTask PrepareAsync()
        {
            if (!File.Exists(AssetFilePath[0]))
                throw new FileNotFoundException("The requested file does not exist.", Path.GetFileName(AssetFilePath[0]));

            var fileHash = new FileStream(AssetFilePath[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite).ToMD5();

            if (AssetFileHash[0].Equals(fileHash))
                return;

            if (Asset[0])
                ReleaseUnmanagedResources();

            using (var www = UnityWebRequestTexture.GetTexture($"{AssetFilePath[0]}"))
            {
                await www.SendWebRequest();

                if (www.isNetworkError)
                    throw new FileLoadException(www.error, Path.GetFileName(AssetFilePath[0]));

                var texture = DownloadHandlerTexture.GetContent(www);

                if (!texture)
                    throw new FileLoadException("Unable to decode Texture2D.", Path.GetFileName(AssetFilePath[0]));

                texture.name = Path.GetFileName(AssetFilePath[0]);

                Asset[0] = texture;
                AssetFileHash[0] = fileHash;
            }
        }

        protected override void ReleaseUnmanagedResources()
        {
            if (Asset[0])
                Object.Destroy(Asset[0]);

            foreach (var texture in RectTextures)
                Object.Destroy(texture.Value);
        }
    }
}