﻿using System.Collections.Generic;

namespace Digimon_3D.Services.Content.Asset.Format
{
    internal sealed class RawFileFormat : FileFormat
    {
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { "" };
    }
}