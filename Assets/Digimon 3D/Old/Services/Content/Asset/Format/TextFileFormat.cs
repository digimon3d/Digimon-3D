﻿using System.Collections.Generic;

namespace Digimon_3D.Services.Content.Asset.Format
{
    internal sealed class TextFileFormat : FileFormat
    {
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".txt", "" };
    }
}