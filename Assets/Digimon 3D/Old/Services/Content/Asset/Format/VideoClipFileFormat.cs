﻿using System.Collections.Generic;

namespace Digimon_3D.Services.Content.Asset.Format
{
    internal sealed class VideoClipFileFormat : FileFormat
    {
#if UNITY_STANDALONE_WIN
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".ogv", ".vp8", ".webm", ".dv", ".m4v", ".mov", ".mp4", ".mpg", ".mpeg", ".asf", ".avi", ".wmv" };
#elif UNITY_STANDALONE_LINUX
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".ogv", ".vp8", ".webm" };
#elif UNITY_STANDALONE_OSX
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".ogv", ".vp8", ".webm", ".dv", ".m4v", ".mov", ".mp4", ".mpg", ".mpeg" };
#endif
    }
}