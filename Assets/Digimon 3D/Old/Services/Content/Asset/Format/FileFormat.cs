﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Digimon_3D.Services.Content.Asset.Format
{
    internal interface IFileFormat
    {
        bool AssertFileFormat(string filePath);
    }

    internal abstract class FileFormat : IFileFormat
    {
        protected abstract IEnumerable<string> SupportedFormats { get; }

        public bool AssertFileFormat(string filePath)
        {
            var extension = Path.GetExtension(filePath);

            foreach (var supportedFormat in SupportedFormats)
            {
                if (string.IsNullOrEmpty(supportedFormat))
                    return true;

                if (extension?.Equals(supportedFormat, StringComparison.OrdinalIgnoreCase) ?? false)
                    return true;
            }

            return false;
        }
    }
}