﻿using System.Collections.Generic;

namespace Digimon_3D.Services.Content.Asset.Format
{
    internal sealed class AudioClipFileFormat : FileFormat
    {
#if UNITY_STANDALONE_WIN
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".ogg", ".wav" };
#else
        protected override IEnumerable<string> SupportedFormats { get; } = new[] { ".ogg" };
#endif
    }
}