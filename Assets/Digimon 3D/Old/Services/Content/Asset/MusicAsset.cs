﻿using System.IO;
using Digimon_3D.Extensions.System.Security.Cryptography;
using Digimon_3D.Services.Content.Asset.Modifier;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;

namespace Digimon_3D.Services.Content.Asset
{
    internal sealed class MusicAsset : GameAsset<AudioClip, MusicModifier>
    {
        public AudioClip MusicLoop => Asset[0];

        public AudioClip MusicIntro => Asset[1];

        public MusicAsset(string[] assetFilePath, MusicModifier assetModifier) : base(assetFilePath, assetModifier)
        {
        }

        public override async UniTask PrepareAsync()
        {
            if (!File.Exists(AssetFilePath[0]))
                throw new FileNotFoundException("The requested file does not exist.", Path.GetFileName(AssetFilePath[0]));

            var fileHash = new FileStream(AssetFilePath[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite).ToMD5();

            if (AssetFileHash[0].Equals(fileHash))
                return;

            if (Asset[0])
                Object.Destroy(Asset[0]);

            using (var www = UnityWebRequestMultimedia.GetAudioClip($"{AssetFilePath[0]}", AudioType.UNKNOWN))
            {
                await www.SendWebRequest();

                if (www.isNetworkError)
                    throw new FileLoadException(www.error, Path.GetFileName(AssetFilePath[0]));

                var audioClip = DownloadHandlerAudioClip.GetContent(www);

                if (!audioClip)
                    throw new FileLoadException("Unable to decode AudioClip.", Path.GetFileName(AssetFilePath[0]));

                audioClip.name = Path.GetFileName(AssetFilePath[0]);

                Asset[0] = audioClip;
                AssetFileHash[0] = fileHash;
            }

            if (!File.Exists(AssetFilePath[1]))
                return;

            fileHash = new FileStream(AssetFilePath[1], FileMode.Open, FileAccess.Read, FileShare.ReadWrite).ToMD5();

            if (AssetFileHash[1].Equals(fileHash))
                return;

            if (Asset[1])
                Object.Destroy(Asset[1]);

            using (var www = UnityWebRequestMultimedia.GetAudioClip($"file:///{AssetFilePath[1]}", AudioType.UNKNOWN))
            {
                await www.SendWebRequest();

                if (www.isNetworkError)
                    throw new FileLoadException(www.error, Path.GetFileName(AssetFilePath[1]));

                var audioClip = DownloadHandlerAudioClip.GetContent(www);

                if (!audioClip)
                    throw new FileLoadException("Unable to decode AudioClip.", Path.GetFileName(AssetFilePath[1]));

                audioClip.name = Path.GetFileName(AssetFilePath[1]);

                Asset[1] = audioClip;
                AssetFileHash[1] = fileHash;
            }
        }

        protected override void ReleaseUnmanagedResources()
        {
            if (Asset[0])
                Object.Destroy(Asset[0]);

            if (Asset[1])
                Object.Destroy(Asset[1]);
        }
    }
}