﻿using System;
using System.Collections.Generic;
using Digimon_3D.Services.Content.Asset.Modifier;
using UniRx.Async;

namespace Digimon_3D.Services.Content.Asset
{
    internal interface IGameAsset : IDisposable
    {
        string[] AssetPointers { get; }

        UniTask PrepareAsync();
    }

    internal abstract class GameAsset<TAsset, TModifier> : IGameAsset where TModifier : BaseModifier
    {
        public string[] AssetPointers { get; }

        public TModifier AssetModifier { get; }

        protected TAsset[] Asset { get; }

        protected string[] AssetFilePath { get; }

        protected string[] AssetFileHash { get; }

        protected GameAsset(string[] assetFilePath, TModifier assetModifier)
        {
            AssetFilePath = assetFilePath;
            AssetModifier = assetModifier;
            AssetPointers = GetAssetPointers();

            Asset = new TAsset[assetFilePath.Length];
            AssetFileHash = new string[assetFilePath.Length];

            for (var i = 0; i < AssetFileHash.Length; i++)
                AssetFileHash[i] = "";
        }

        public abstract UniTask PrepareAsync();

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }

        protected abstract void ReleaseUnmanagedResources();

        private string[] GetAssetPointers()
        {
            var assetPointers = new List<string> { AssetModifier.AssetPointer };

            foreach (var assetAliasPointer in AssetModifier.AssetAliasPointers)
            {
                if (assetPointers.Contains(assetAliasPointer))
                    continue;

                assetPointers.Add(assetAliasPointer);
            }

            return assetPointers.ToArray();
        }

        ~GameAsset()
        {
            ReleaseUnmanagedResources();
        }
    }
}