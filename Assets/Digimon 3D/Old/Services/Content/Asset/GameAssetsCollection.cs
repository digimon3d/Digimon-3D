﻿using System;
using System.Collections.Generic;
using System.IO;
using Digimon_3D.Extensions.System.IO;
using Digimon_3D.Services.Content.Asset.Format;
using Digimon_3D.Services.Content.Asset.Modifier;
using Digimon_3D.Services.IO;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Services.Content.Asset
{
    internal sealed class GameAssetsCollection
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<GameAssetsCollectionConfig, GameAssetsCollection>
        {
        }

        public List<IGameAsset> Assets { get; } = new List<IGameAsset>();

        private GameAssetsCollectionConfig GameAssetsCollectionConfig { get; }

        private AssetModifier AssetModifier { get; set; }

        private DirectoryService DirectoryService { get; }

        public GameAssetsCollection(GameAssetsCollectionConfig gameAssetsCollectionConfig, DirectoryService directoryService)
        {
            GameAssetsCollectionConfig = gameAssetsCollectionConfig;
            DirectoryService = directoryService;

            LoadAssetModifier(GameAssetsCollectionConfig.AssetModifierFile);
            LoadGameAssets(GameAssetsCollectionConfig.ContentFolder);
        }

        private static T GetModifier<T>(string assetPointer, IEnumerable<T> assetModifiers) where T : BaseModifier, new()
        {
            foreach (var assetModifier in assetModifiers)
            {
                if (assetModifier.AssetPointer.Equals(assetPointer, StringComparison.OrdinalIgnoreCase))
                    return assetModifier;
            }

            return new T { AssetPointer = assetPointer };
        }

        private static IEnumerable<string> GetAssetFiles(string assetFolder, SearchOption searchOption = SearchOption.AllDirectories)
        {
            if (!Directory.Exists(assetFolder.ResolveFullPath()))
                return new List<string>();

            var assetFiles = Directory.GetFiles(assetFolder.ResolveFullPath(), "*.*", searchOption);

            return assetFiles;
        }

        private void LoadAssetModifier(string assetModifierFile)
        {
            AssetModifier = new AssetModifier();

            if (string.IsNullOrEmpty(assetModifierFile) || !File.Exists(assetModifierFile))
                return;

            using (var reader = new StreamReader(new FileStream(assetModifierFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                JsonUtility.FromJsonOverwrite(reader.ReadToEnd(), AssetModifier);
        }

        private void LoadGameAssets(string contentFolder)
        {
            // AssetsBundle
            // Localization

            // Music
            var musicAssetFiles = GetAssetFiles(string.Format(DirectoryService.MusicContentDirectoryFormat, contentFolder).ResolveFullPath(), SearchOption.TopDirectoryOnly);
            LoadGameAssets<MusicAsset, MusicModifier, AudioClipFileFormat>(contentFolder, musicAssetFiles, AssetModifier.MusicModifiers);

            // Sound
            var soundAssetFiles = GetAssetFiles(string.Format(DirectoryService.SoundContentDirectoryFormat, contentFolder).ResolveFullPath());
            LoadGameAssets<SoundAsset, SoundModifier, AudioClipFileFormat>(contentFolder, soundAssetFiles, AssetModifier.SoundModifiers);

            // Texture
            var textureAssetFiles = GetAssetFiles(string.Format(DirectoryService.TextureContentDirectoryFormat, contentFolder).ResolveFullPath());
            LoadGameAssets<TextureAsset, TextureModifier, Texture2DFileFormat>(contentFolder, textureAssetFiles, AssetModifier.TextureModifiers);

            // Video
            var videoAssetFiles = GetAssetFiles(string.Format(DirectoryService.VideoContentDirectoryFormat, contentFolder).ResolveFullPath());
            LoadGameAssets<VideoAsset, BaseModifier, VideoClipFileFormat>(contentFolder, videoAssetFiles, AssetModifier.VideoModifiers);
        }

        private void LoadGameAssets<TAsset, TModifier, TAssetFormat>(string contentFolder, IEnumerable<string> assetFiles, TModifier[] modifiers) where TAsset : IGameAsset where TModifier : BaseModifier, new() where TAssetFormat : IFileFormat, new()
        {
            foreach (var assetFile in assetFiles)
            {
                IFileFormat fileFormat = new TAssetFormat();

                if (!fileFormat.AssertFileFormat(assetFile))
                    continue;

                var assetPointer = assetFile.Replace($"{contentFolder}/".ResolveFilePath(), "");
                assetPointer = assetPointer.Remove(assetPointer.LastIndexOf('.'));

                var modifier = GetModifier(assetPointer, modifiers);

                if (typeof(TAsset) == typeof(MusicAsset))
                {
                    var introAssetFile = assetFile.Replace(string.Format(DirectoryService.MusicContentDirectoryFormat, contentFolder).ResolveFullPath(), string.Format(DirectoryService.MusicIntroContentDirectoryFormat, contentFolder).ResolveFullPath());
                    Assets.Add(new MusicAsset(new[] { assetFile, introAssetFile }, modifier as MusicModifier));
                }
                else if (typeof(TAsset) == typeof(SoundAsset))
                    Assets.Add(new SoundAsset(new[] { assetFile }, modifier as SoundModifier));
                else if (typeof(TAsset) == typeof(TextureAsset))
                    Assets.Add(new TextureAsset(new[] { assetFile }, modifier as TextureModifier));
                else if (typeof(TAsset) == typeof(VideoAsset))
                    Assets.Add(new VideoAsset(new[] { assetFile }, modifier));
            }
        }
    }
}