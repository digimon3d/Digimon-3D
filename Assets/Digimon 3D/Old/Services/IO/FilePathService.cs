﻿using Digimon_3D.Extensions.System.IO;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.IO
{
    [UsedImplicitly]
    internal sealed class FilePathService : IInitializable
    {
        public string LoggerFilePath { get; private set; }

        public string ApplicationSettingFilePath { get; private set; }

        public string InputSettingFilePath { get; private set; }

        public string AssetModifierFilePathFormat { get; private set; }

        public string ContentPackFilePathFormat { get; private set; }

        public string GameModeFilePathFormat { get; private set; }

        public string GameModeDataEntityDefinitionFilePathFormat { get; private set; }

        private DirectoryService DirectoryService { get; }

        public FilePathService(DirectoryService directoryService)
        {
            DirectoryService = directoryService;
        }

        public void Initialize()
        {
            LoggerFilePath = $"{DirectoryService.StreamingAssetsDirectory}/Logger.log".ResolveFullPath();
            ApplicationSettingFilePath = $"{DirectoryService.StreamingAssetsDirectory}/ApplicationSetting.json".ResolveFullPath();
            InputSettingFilePath = $"{DirectoryService.StreamingAssetsDirectory}/InputSetting.json".ResolveFullPath();
            AssetModifierFilePathFormat = "{0}/ContentModifier.json".ResolveFilePath();
            ContentPackFilePathFormat = "{0}/ContentPack.json".ResolveFilePath();
            GameModeFilePathFormat = "{0}/GameMode.json".ResolveFilePath();
            GameModeDataEntityDefinitionFilePathFormat = "{0}/EntityDefinition.json".ResolveFilePath();
        }
    }
}