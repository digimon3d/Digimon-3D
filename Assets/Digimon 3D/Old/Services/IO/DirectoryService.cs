﻿using System.IO;
using Digimon_3D.Extensions.System.IO;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.IO
{
    [UsedImplicitly]
    internal sealed class DirectoryService : IInitializable
    {
        public string AppDataDirectory { get; private set; }

        public string StreamingAssetsDirectory { get; private set; }

        public string ContentDirectory { get; private set; }

        public string AssetsBundleContentDirectoryFormat { get; private set; }

        public string AssetsBundleContentDirectoryPointerFormat { get; private set; }

        public string LocalizationContentDirectoryFormat { get; private set; }

        public string LocalizationContentDirectoryPointerFormat { get; private set; }

        public string MusicContentDirectoryFormat { get; private set; }

        public string MusicIntroContentDirectoryFormat { get; private set; }

        public string MusicContentDirectoryPointerFormat { get; private set; }

        public string SoundContentDirectoryFormat { get; private set; }

        public string SoundContentDirectoryPointerFormat { get; private set; }

        public string TextureContentDirectoryFormat { get; private set; }

        public string TextureContentDirectoryPointerFormat { get; private set; }

        public string VideoContentDirectoryFormat { get; private set; }

        public string VideoContentDirectoryPointerFormat { get; private set; }

        public string ContentPackDirectory { get; private set; }

        public string GameModeDirectory { get; private set; }

        public void Initialize()
        {
            AppDataDirectory = UnityEngine.Application.persistentDataPath.ResolveFullPath();

#if UNITY_EDITOR
            StreamingAssetsDirectory = $"{UnityEngine.Application.dataPath}/../Bin/Debug/Digimon 3D_Data/StreamingAssets".ResolveFullPath();

            if (!Directory.Exists(StreamingAssetsDirectory))
                throw new FileNotFoundException("Unable to find debug assets. Ensure you have built debug assets via Digimon 3D > Build > Debug.");
#else
            StreamingAssetsDirectory = UnityEngine.Application.streamingAssetsPath.ResolveFullPath();
#endif

            // Content
            ContentDirectory = $"{StreamingAssetsDirectory}/Content".ResolveFullPath();

            // AssetsBundle
            AssetsBundleContentDirectoryFormat = "{0}/AssetsBundle".ResolveFilePath();
            AssetsBundleContentDirectoryPointerFormat = "AssetsBundle/{0}".ResolveFilePath();

            // Localization
            LocalizationContentDirectoryFormat = "{0}/Localization".ResolveFilePath();
            LocalizationContentDirectoryPointerFormat = "Localization/{0}".ResolveFilePath();

            // Music
            MusicContentDirectoryFormat = "{0}/Music".ResolveFilePath();
            MusicIntroContentDirectoryFormat = "{0}/Music/Intro".ResolveFilePath();
            MusicContentDirectoryPointerFormat = "Music/{0}".ResolveFilePath();

            // Sound
            SoundContentDirectoryFormat = "{0}/Sound".ResolveFilePath();
            SoundContentDirectoryPointerFormat = "Sound/{0}".ResolveFilePath();

            // Texture
            TextureContentDirectoryFormat = "{0}/Texture".ResolveFilePath();
            TextureContentDirectoryPointerFormat = "Texture/{0}".ResolveFilePath();

            // Video
            VideoContentDirectoryFormat = "{0}/Video".ResolveFilePath();
            VideoContentDirectoryPointerFormat = "Video/{0}".ResolveFilePath();

            // Content Pack
            ContentPackDirectory = $"{StreamingAssetsDirectory}/ContentPack".ResolveFullPath();

            // GameMode
            GameModeDirectory = $"{StreamingAssetsDirectory}/GameMode".ResolveFullPath();
        }
    }
}