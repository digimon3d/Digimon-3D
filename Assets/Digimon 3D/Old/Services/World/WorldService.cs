﻿using System;
using Digimon_3D.Extensions.System;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Services.World
{
    [UsedImplicitly]
    internal sealed class WorldService : ITickable
    {
        public int SecondsInADay { get; } = 24 * 60 * 60;

        public int CurrentTime { get; private set; }

        public int TimeOffset { get; set; } = 0;

        public int GameHour => (int) (GameClock / SecondsInADay * 24 % 24);

        public int GameMinute => (int) (GameClock / SecondsInADay * 24 * 60 % 60);

        public int GameSecond => (int) (GameClock / SecondsInADay * 24 * 60 * 60 % 60);

        private float GameClock { get; set; }

        public void Tick()
        {
            CurrentTime = (DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second + TimeOffset).RollOver(0, SecondsInADay);
            GameClock = (DateTime.Now.Day - 1) * SecondsInADay + CurrentTime;
        }
    }
}