﻿using System;
using Digimon_3D.Views;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Scenes
{
    internal interface IScene
    {
        GameObject SceneGameObject { get; }
    }

    internal abstract class BaseScene : MonoBehaviour, IScene
    {
        public GameObject SceneGameObject => gameObject;

        protected TView AddViewOnScene<TView>(IFactory<TView> viewFactory) where TView : MonoBehaviour, IView
        {
            var newView = viewFactory.Create();

            if (newView == null)
                throw new NullReferenceException();

            newView.transform.SetParent(transform);

            return newView;
        }
    }
}