﻿using Digimon_3D.Services.Audio;
using Digimon_3D.Services.Debug;
using Digimon_3D.Views.Debug;
using Digimon_3D.Views.MainMenu;
using Digimon_3D.Views.World;
using JetBrains.Annotations;
using UniRx.Async;
using Zenject;

namespace Digimon_3D.Scenes.MainMenu
{
    internal sealed class MainMenuScene : BaseScene
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<MainMenuScene>
        {
        }

        [Inject]
        private DebugService DebugService { get; }

        [Inject]
        private MusicPlayerService MusicPlayerService { get; }

        [Inject]
        private DebugView.Factory DebugViewFactory { get; }

        [Inject]
        private WorldView.Factory WorldViewFactory { get; }

        [Inject]
        private MainMenuView.Factory MainMenuViewFactory { get; }

        private DebugView DebugView { get; set; }

        private WorldView WorldView { get; set; }

        private MainMenuView MainMenuView { get; set; }

        [Inject]
        private async UniTaskVoid Initialize()
        {
            DebugView = AddViewOnScene(DebugViewFactory);
            WorldView = AddViewOnScene(WorldViewFactory);
            MainMenuView = AddViewOnScene(MainMenuViewFactory);

            DebugService.CanShowDebugMode = true;
            DebugService.CanShowDeveloper = true;

            await MusicPlayerService.PlayAsync("Title");
        }
    }
}