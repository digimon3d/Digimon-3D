﻿using Digimon_3D.Services.Audio;
using Digimon_3D.Services.Debug;
using Digimon_3D.Views.Debug;
using Digimon_3D.Views.World;
using JetBrains.Annotations;
using UniRx.Async;
using Zenject;

namespace Digimon_3D.Scenes.Overworld
{
    internal sealed class OverworldScene : BaseScene
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<OverworldScene>
        {
        }

        [Inject]
        private DebugService DebugService { get; }

        [Inject]
        private MusicPlayerService MusicPlayerService { get; }

        [Inject]
        private DebugView.Factory DebugViewFactory { get; }

        [Inject]
        private WorldView.Factory WorldViewFactory { get; }

        private DebugView DebugView { get; set; }

        private WorldView WorldView { get; set; }

        [Inject]
        private async UniTaskVoid Initialize()
        {
            DebugView = AddViewOnScene(DebugViewFactory);
            WorldView = AddViewOnScene(WorldViewFactory);

            DebugService.CanShowDebugMode = true;
            DebugService.CanShowDeveloper = false;

            await MusicPlayerService.PlayAsync("Title");
        }
    }
}