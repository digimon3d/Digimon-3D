﻿using Digimon_3D.Views.Splash;
using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Scenes.Splash
{
    internal sealed class SplashScene : BaseScene
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<SplashScene>
        {
        }

        [Inject]
        private SplashView.Factory SplashViewFactory { get; }

        private SplashView SplashView { get; set; }

        [Inject]
        private void Initialize()
        {
            SplashView = AddViewOnScene(SplashViewFactory);
        }
    }
}