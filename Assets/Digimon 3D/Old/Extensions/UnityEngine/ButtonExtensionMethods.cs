﻿using UnityEngine;
using UnityEngine.UI;

namespace Digimon_3D.Extensions.UnityEngine
{
    public static class ButtonExtensionMethods
    {
        public static Button CreateButton(this GameObject gameObject, Texture2D baseTexture, Texture2D highlightedTexture, Texture2D pressedTexture, Texture2D disabledTexture)
        {
            var button = gameObject.GetComponent<Button>();
            var buttonImage = gameObject.GetComponent<Image>();

            buttonImage.sprite = baseTexture.CreateSprite(true);
            buttonImage.type = Image.Type.Sliced;
            button.spriteState = new SpriteState { highlightedSprite = highlightedTexture.CreateSprite(true), pressedSprite = pressedTexture.CreateSprite(true), disabledSprite = disabledTexture.CreateSprite(true) };

            return button;
        }
    }
}