﻿using System;
using UnityEngine;

namespace Digimon_3D.Extensions.UnityEngine
{
    public static class SpriteExtensionMethods
    {
        public static Sprite CreateSprite(this Texture2D texture)
        {
            if (texture == null)
                throw new ArgumentNullException(nameof(texture));

            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2.0f, texture.height / 2.0f), 32, 0, SpriteMeshType.FullRect, Vector4.zero);
        }

        public static Sprite CreateSprite(this Texture2D texture, bool border)
        {
            if (texture == null)
                throw new ArgumentNullException(nameof(texture));

            return border ? Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2.0f, texture.height / 2.0f), 32, 0, SpriteMeshType.FullRect, new Vector4(texture.width / 3.0f, texture.height / 3.0f, texture.width / 3.0f, texture.height / 3.0f)) : Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2.0f, texture.height / 2.0f), 32, 0, SpriteMeshType.FullRect, Vector4.zero);
        }
    }
}