﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Digimon_3D.Extensions.System.Security.Cryptography
{
    public static class CryptographyExtensionMethods
    {
        private const int Keysize = 256;

        private const int DerivationIterations = 1000;

        public static string EncryptString(this string plainText, string passPhrase)
        {
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                using (var symmetricKey = new RijndaelManaged())
                {
                    var keyBytes = password.GetBytes(Keysize / 8);

                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;

                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        MemoryStream memoryStream;

                        using (var cryptoStream = new CryptoStream(memoryStream = new MemoryStream(), encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                            cryptoStream.FlushFinalBlock();

                            var finalResult = new byte[saltStringBytes.Length + ivStringBytes.Length + memoryStream.Length];

                            Buffer.BlockCopy(saltStringBytes, 0, finalResult, 0, saltStringBytes.Length);
                            Buffer.BlockCopy(ivStringBytes, 0, finalResult, saltStringBytes.Length, ivStringBytes.Length);
                            Buffer.BlockCopy(memoryStream.ToArray(), 0, finalResult, saltStringBytes.Length + ivStringBytes.Length, Convert.ToInt32(memoryStream.Length));

                            return Convert.ToBase64String(finalResult);
                        }
                    }
                }
            }
        }

        public static string DecryptString(this string cipherText, string passPhrase)
        {
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            var saltStringBytes = new byte[32];
            var ivStringBytes = new byte[32];
            var cipherTextBytes = new byte[cipherTextBytesWithSaltAndIv.Length - 64];

            Buffer.BlockCopy(cipherTextBytesWithSaltAndIv, 0, saltStringBytes, 0, 32);
            Buffer.BlockCopy(cipherTextBytesWithSaltAndIv, 32, ivStringBytes, 0, 32);
            Buffer.BlockCopy(cipherTextBytesWithSaltAndIv, 64, cipherTextBytes, 0, cipherTextBytesWithSaltAndIv.Length - 64);

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                using (var symmetricKey = new RijndaelManaged())
                {
                    var keyBytes = password.GetBytes(Keysize / 8);

                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;

                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var cryptoStream = new CryptoStream(new MemoryStream(cipherTextBytes), decryptor, CryptoStreamMode.Read))
                        {
                            var plainTextBytes = new byte[cipherTextBytes.Length];
                            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32];

            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetBytes(randomBytes);

                return randomBytes;
            }
        }
    }
}