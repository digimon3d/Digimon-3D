﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Digimon_3D.Extensions.System.Security.Cryptography
{
    public static class MD5ExtensionMethods
    {
        public static string ToMD5(this string value)
        {
            using (var md5 = MD5.Create())
            {
                var hashBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
                var stringBuilder = new StringBuilder();

                foreach (var hashByte in hashBytes)
                    stringBuilder.Append(hashByte.ToString("X2"));

                return stringBuilder.ToString();
            }
        }

        public static string ToMD5(this Stream stream)
        {
            using (stream)
            {
                if (stream.CanSeek)
                    stream.Seek(0, SeekOrigin.Begin);

                using (var md5 = MD5.Create())
                {
                    var hashBytes = md5.ComputeHash(stream);
                    var stringBuilder = new StringBuilder();

                    foreach (var hashByte in hashBytes)
                        stringBuilder.Append(hashByte.ToString("X2"));

                    return stringBuilder.ToString();
                }
            }
        }
    }
}