﻿namespace Digimon_3D.Extensions.System
{
    public static class MathExtensionMethods
    {
        public static byte Clamp(this byte value, byte minValue, byte maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static sbyte Clamp(this sbyte value, sbyte minValue, sbyte maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static short Clamp(this short value, short minValue, short maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static ushort Clamp(this ushort value, ushort minValue, ushort maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static int Clamp(this int value, int minValue, int maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static uint Clamp(this uint value, uint minValue, uint maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static long Clamp(this long value, long minValue, long maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static ulong Clamp(this ulong value, ulong minValue, ulong maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static float Clamp(this float value, float minValue, float maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static double Clamp(this double value, double minValue, double maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static decimal Clamp(this decimal value, decimal minValue, decimal maxValue)
        {
            if (value < minValue)
                return minValue;

            return value > maxValue ? maxValue : value;
        }

        public static int RollOver(this int value, int minValue, int maxValue)
        {
            var difference = maxValue - minValue + 1;
            var newValue = value;

            if (value > maxValue)
            {
                while (newValue > maxValue)
                    newValue -= difference;
            }
            else if (value < minValue)
            {
                while (newValue < maxValue)
                    newValue += difference;
            }

            return newValue;
        }

        public static long RollOver(this long value, long minValue, long maxValue)
        {
            var difference = maxValue - minValue + 1;
            var newValue = value;

            if (value > maxValue)
            {
                while (newValue > maxValue)
                    newValue -= difference;
            }
            else if (value < minValue)
            {
                while (newValue < maxValue)
                    newValue += difference;
            }

            return newValue;
        }
    }
}