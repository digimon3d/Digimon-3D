﻿using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Views.Debug
{
    internal sealed class DebugView : BaseView
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<DebugView>
        {
        }

        [Inject]
        private DebugComponent.Factory DebugComponentFactory { get; }

        private DebugComponent DebugComponent { get; set; }

        [Inject]
        private void Initialize()
        {
            DebugComponent = DebugComponentFactory.Create(this);
        }
    }
}