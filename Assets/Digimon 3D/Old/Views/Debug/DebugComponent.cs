﻿using System.Globalization;
using Digimon_3D.Services.Debug;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Digimon_3D.Views.Debug
{
    internal sealed class DebugComponent : BaseComponent<DebugView>
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<DebugView, DebugComponent>
        {
        }

        [SerializeField]
        private Text _gameNameText;

        [SerializeField]
        private Text _fpsText;

        [SerializeField]
        private Text _debugModeText;

        [SerializeField]
        private Text _additionalDebugText;

        [SerializeField]
        private Text _bottomText;

        [Inject]
        private DebugService DebugService { get; }

        private string GameNameText { get; set; }

        private string GameNameTextWithFPS { get; set; }

        private string DebugModeText { get; set; }

        private string UnknownDebugInfoText { get; set; }

        [Inject]
        private void Initialize()
        {
            GameNameText = $"{Game.Name} {Game.Version}";
            GameNameTextWithFPS = $"{Game.Name} {Game.Version} / FPS: ";
            DebugModeText = " (Debug Mode)";
            UnknownDebugInfoText = "=== No debug information available ===";
        }

        private void Update()
        {
            _gameNameText.text = GetGameNameText();
            _fpsText.text = GetFPSText();
            _debugModeText.text = GetDebugModeText();
            _additionalDebugText.text = GetAdditionalDebugModeText();
            _bottomText.text = GetBottomText();
        }

        private string GetGameNameText()
        {
            return DebugService.DebugMode == DebugModes.None ? GameNameText : GameNameTextWithFPS;
        }

        private string GetFPSText()
        {
            return DebugService.DebugMode == DebugModes.None ? string.Empty : DebugService.CurrentFPS.ToString(CultureInfo.CurrentCulture);
        }

        private string GetDebugModeText()
        {
            return Game.IsDebugMode ? DebugModeText : string.Empty;
        }

        private string GetAdditionalDebugModeText()
        {
            if (DebugService.DebugMode != DebugModes.Detailed)
                return string.Empty;

            var debugString = DebugService.GetDebugString();
            return string.IsNullOrWhiteSpace(debugString) ? UnknownDebugInfoText : debugString;
        }

        private string GetBottomText()
        {
            return DebugService.CanShowDeveloper ? Game.Developer : string.Empty;
        }
    }
}