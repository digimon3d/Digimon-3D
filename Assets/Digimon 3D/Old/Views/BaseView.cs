﻿using UnityEngine;

namespace Digimon_3D.Views
{
    internal interface IView
    {
        GameObject ViewGameObject { get; }
    }

    internal abstract class BaseView : MonoBehaviour, IView
    {
        public GameObject ViewGameObject => gameObject;
    }
}