﻿using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Views.Splash
{
    internal sealed class SplashView : BaseView
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<SplashView>
        {
        }

        [Inject]
        private SplashComponent.Factory SplashComponentFactory { get; }

        private SplashComponent SplashComponent { get; set; }

        [Inject]
        private void Initialize()
        {
            SplashComponent = SplashComponentFactory.Create(this);
        }
    }
}