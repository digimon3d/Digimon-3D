﻿using Digimon_3D.Scenes.MainMenu;
using Digimon_3D.Services.Scene;
using Digimon_3D.Services.Video;
using JetBrains.Annotations;
using UniRx.Async;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Views.Splash
{
    [UsedImplicitly]
    internal sealed class SplashComponent : BaseComponent<SplashView>
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<SplashView, SplashComponent>
        {
        }

        [Inject]
        private VideoPlayerService VideoPlayerService { get; }

        [Inject]
        private ISceneService SceneService { get; }

        [Inject]
        private void Initialize()
        {
            InitializeAsync().Forget();
        }

        private async UniTaskVoid InitializeAsync()
        {
            await VideoPlayerService.PlayAsync("Digimon 3D Intro");
            await UniTask.WaitWhile(() => VideoPlayerService.Status != VideoStatus.Stopped && !Input.anyKeyDown);
            VideoPlayerService.Stop();

            SceneService.ChangeScene<MainMenuScene>();
        }
    }
}