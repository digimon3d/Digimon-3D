﻿using UnityEngine;
using Zenject;

namespace Digimon_3D.Views
{
    internal interface IComponent
    {
        GameObject ComponentGameObject { get; }
    }

    internal abstract class BaseComponent<TView> : MonoBehaviour, IComponent where TView : IView
    {
        public GameObject ComponentGameObject => gameObject;

        protected TView View { get; private set; }

        [Inject]
        public void Constructor(TView view)
        {
            View = view;
            transform.SetParent(View.ViewGameObject.transform);
        }
    }
}