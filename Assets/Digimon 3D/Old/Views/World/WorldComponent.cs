﻿using Digimon_3D.Services.World;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Digimon_3D.Views.World
{
    internal sealed class WorldComponent : BaseComponent<WorldView>
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<WorldView, WorldComponent>
        {
        }

        [SerializeField]
        private Transform[] _cloudSpheres;

        [SerializeField]
        private Transform[] _starSpheres;

        [SerializeField]
        private float _angleNorthDegrees;

        [SerializeField]
        private float _declinationDegrees = 60;

        [SerializeField]
        private float _winterVsSummer;

        [SerializeField]
        private float _moonPhase = 135;

        [SerializeField]
        private float _maximumSunIntensity = 1;

        [SerializeField]
        private float _maximumMoonIntensity = 0.5f;

        [SerializeField]
        private int _hourTheSunIsAtTheTop = 14;

        [SerializeField]
        private GameObject _dayNightCycleObject;

        [SerializeField]
        private GameObject _sunObject;

        [SerializeField]
        private Light _sunLight;

        [SerializeField]
        private GameObject _moonBody;

        [SerializeField]
        private GameObject _sunTarget;

        [Inject]
        private WorldService WorldService { get; }

        private float TwinkleCounter { get; set; }

        private int LastSphereRenderedHour { get; set; }

        private int LastSphereRenderedMinute { get; set; }

        private int LastSphereRenderedSecond { get; set; }

        [Inject]
        private void Initialize()
        {
            _moonBody.transform.RotateAround(_sunTarget.transform.position, Vector3.right, _moonPhase);
            _moonBody.transform.RotateAround(_sunTarget.transform.position, Vector3.forward, 6);

            _maximumMoonIntensity = _maximumMoonIntensity * (1 - Mathf.Abs((180 - _moonPhase) / 180));

            _dayNightCycleObject.transform.rotation = Quaternion.identity;
            _dayNightCycleObject.transform.position = _sunTarget.transform.position + Vector3.up * _winterVsSummer * transform.localScale.magnitude * 0.5f;
            _dayNightCycleObject.transform.Rotate(Vector3.up * _angleNorthDegrees);
            _dayNightCycleObject.transform.Rotate(Vector3.forward * _declinationDegrees);

            LastSphereRenderedHour = _hourTheSunIsAtTheTop;
            LastSphereRenderedMinute = 0;
            LastSphereRenderedSecond = 0;
        }

        private void Update()
        {
            foreach (var cloud in _cloudSpheres)
                cloud.transform.Rotate(Vector3.forward * Time.deltaTime);

            Color currentColor;

            foreach (var stars in _starSpheres)
            {
                stars.transform.Rotate(Vector3.forward * Time.deltaTime);

                if (WorldService.CurrentTime <= 7 * 60 * 60 || WorldService.CurrentTime >= 21 * 60 * 60)
                    continue;

                var starRenderer = stars.GetComponent<Renderer>();
                currentColor = starRenderer.material.color;
                starRenderer.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, Mathf.Lerp(currentColor.a, 0.0f, Time.deltaTime * 50.0f));
            }

            var chosenOne = Random.Range(0, _starSpheres.Length);

            if (_starSpheres[chosenOne] && TwinkleCounter <= 0.0f && (WorldService.CurrentTime <= 7 * 60 * 60 || WorldService.CurrentTime >= 21 * 60 * 60))
            {
                var starRenderer = _starSpheres[chosenOne].GetComponent<Renderer>();

                TwinkleCounter = 1.0f;
                currentColor = starRenderer.material.color;
                starRenderer.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, Random.Range(0.01f, 0.5f));
            }

            if (TwinkleCounter > 0.0f)
                TwinkleCounter -= Time.deltaTime * 10;

            // Day Night Cycle
            _sunLight.intensity = (Vector3.Project(_sunObject.transform.position.normalized, Vector3.up) * _maximumSunIntensity).y;

            var hour = WorldService.GameHour;
            var minute = WorldService.GameMinute;
            var second = WorldService.GameSecond;
            var timeAdvanced = (hour - LastSphereRenderedHour) * 0.04166667f + (minute - LastSphereRenderedMinute) * 0.0006944f + (second - LastSphereRenderedSecond) * 0.0000115f;

            if (timeAdvanced < 0)
                timeAdvanced += 1;

            _dayNightCycleObject.transform.Rotate(-Vector3.right * timeAdvanced * 360);

            LastSphereRenderedHour = hour;
            LastSphereRenderedMinute = minute;
            LastSphereRenderedSecond = second;

            _moonBody.transform.LookAt(_sunTarget.transform);
            _sunObject.transform.LookAt(_sunTarget.transform);
        }
    }
}