﻿using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Views.World
{
    internal sealed class WorldView : BaseView
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<WorldView>
        {
        }

        [Inject]
        private WorldComponent.Factory WorldComponentFactory { get; }

        private WorldComponent WorldComponent { get; set; }

        [Inject]
        private void Initialize()
        {
            WorldComponent = WorldComponentFactory.Create(this);
        }
    }
}