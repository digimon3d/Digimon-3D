﻿using JetBrains.Annotations;
using Zenject;

namespace Digimon_3D.Views.MainMenu
{
    internal sealed class MainMenuView : BaseView
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<MainMenuView>
        {
        }

        [Inject]
        private LoginScreenComponent.Factory LoginScreenComponentFactory { get; }

        private LoginScreenComponent LoginScreenComponent { get; set; }

        [Inject]
        private void Initialize()
        {
            LoginScreenComponent = LoginScreenComponentFactory.Create(this);
        }
    }
}