﻿using Digimon_3D.Extensions.UnityEngine;
using Digimon_3D.Service.Setting.Application;
using Digimon_3D.Services.Content;
using JetBrains.Annotations;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Digimon_3D.Views.MainMenu
{
    [UsedImplicitly]
    internal sealed class LoginScreenComponent : BaseComponent<MainMenuView>
    {
        [UsedImplicitly]
        internal sealed class Factory : PlaceholderFactory<MainMenuView, LoginScreenComponent>
        {
        }

        [SerializeField]
        private GameObject _logoGameObject;

        [SerializeField]
        private Image _logoImage;

        [SerializeField]
        private GameObject _loginWindowGameObject;

        [SerializeField]
        private Image _loginWindowImage;

        [SerializeField]
        private Image _loginWindowBackgroundMaskImage;

        [SerializeField]
        private Image[] _loginWindowBackgroundMaskBackgroundImage;

        [SerializeField]
        private Text _loginWindowLoginToGameJoltText;

        [SerializeField]
        private Text _loginWindowUsernameText;

        [SerializeField]
        private InputField _loginWindowUsernameInputField;

        [SerializeField]
        private Text _loginWindowTokenText;

        [SerializeField]
        private GameObject _loginWindowTokenButtonGameObject;

        [SerializeField]
        private InputField _loginWindowTokenInputField;

        [SerializeField]
        private GameObject _loginWindowLoginButtonGameObject;

        [SerializeField]
        private Text _loginWindowLoginButtonText;

        [SerializeField]
        private GameObject _loginWindowPlayOfflineButtonGameObject;

        [SerializeField]
        private Text _loginWindowPlayOfflineButtonText;

        [SerializeField]
        private GameObject _offlineWarningWindowGameObject;

        [SerializeField]
        private Image _offlineWarningWindowBackgroundImage;

        [SerializeField]
        private Text _offlineWarningWindowPlayingOfflineText;

        [SerializeField]
        private GameObject _offlineWarningWindowNoButtonGameObject;

        [SerializeField]
        private Text _offlineWarningWindowNoButtonText;

        [SerializeField]
        private GameObject _offlineWarningWindowYesButtonGameObject;

        [SerializeField]
        private Text _offlineWarningWindowYesButtonText;

        [Inject]
        private ContentService ContentService { get; }

        [Inject]
        private ApplicationSettingService ApplicationSettingsService { get; }

        [Inject]
        private void Initialize()
        {
            InitializeAsync().Forget();
        }

        private async UniTaskVoid InitializeAsync()
        {
            var logoImage = await ContentService.GetTextureAssetAsync("Logo/Digimon 3D Logo", false);
            _logoImage.sprite = logoImage.Texture.CreateSprite();
            _logoGameObject.SetActive(true);

            var guiSkins = await ContentService.GetTextureAssetAsync("GUI/GUI Skins", false);

            // Login Window Component
            _loginWindowImage.sprite = guiSkins.GetTexture(new RectInt(0, 32, 32, 32)).CreateSprite(true);
            _loginWindowImage.type = Image.Type.Sliced;

            _loginWindowBackgroundMaskImage.sprite = guiSkins.GetTexture(new RectInt(32, 32, 32, 32)).CreateSprite(true);
            _loginWindowBackgroundMaskImage.type = Image.Type.Sliced;

            foreach (var image in _loginWindowBackgroundMaskBackgroundImage)
                image.sprite = guiSkins.GetTexture(new RectInt(64, 32, 32, 32)).CreateSprite();

            var loginWindowTokenButton = _loginWindowTokenButtonGameObject.CreateButton(guiSkins.GetTexture(new RectInt(32, 64, 32, 32)), guiSkins.GetTexture(new RectInt(64, 64, 32, 32)), guiSkins.GetTexture(new RectInt(96, 64, 32, 32)), guiSkins.GetTexture(new RectInt(0, 64, 32, 32)));
            loginWindowTokenButton.onClick.AddListener(() => Application.OpenURL("https://help.gamejolt.com/tokens"));

            var loginWindowLoginButton = _loginWindowLoginButtonGameObject.CreateButton(guiSkins.GetTexture(new RectInt(32, 64, 32, 32)), guiSkins.GetTexture(new RectInt(64, 64, 32, 32)), guiSkins.GetTexture(new RectInt(96, 64, 32, 32)), guiSkins.GetTexture(new RectInt(0, 64, 32, 32)));

            var loginWindowPlayOfflineButton = _loginWindowPlayOfflineButtonGameObject.CreateButton(guiSkins.GetTexture(new RectInt(32, 64, 32, 32)), guiSkins.GetTexture(new RectInt(64, 64, 32, 32)), guiSkins.GetTexture(new RectInt(96, 64, 32, 32)), guiSkins.GetTexture(new RectInt(0, 64, 32, 32)));
            loginWindowPlayOfflineButton.onClick.AddListener(() =>
            {
                if (ApplicationSettingsService.ShouldDisplayOfflineWarning)
                    _offlineWarningWindowGameObject.SetActive(true);

                //else
                //    Controller.GotoProfileSelectionComponent(this);
            });

            _loginWindowGameObject.SetActive(true);

            // Offline Warning Window Component
            _offlineWarningWindowBackgroundImage.sprite = guiSkins.GetTexture(new RectInt(32, 64, 32, 32)).CreateSprite(true);
            _offlineWarningWindowBackgroundImage.type = Image.Type.Sliced;

            var offlineWarningWindowNoButton = _offlineWarningWindowNoButtonGameObject.CreateButton(guiSkins.GetTexture(new RectInt(32, 64, 32, 32)), guiSkins.GetTexture(new RectInt(64, 64, 32, 32)), guiSkins.GetTexture(new RectInt(96, 64, 32, 32)), guiSkins.GetTexture(new RectInt(0, 64, 32, 32)));
            offlineWarningWindowNoButton.onClick.AddListener(() => _offlineWarningWindowGameObject.SetActive(false));

            var offlineWarningWindowYesButton = _offlineWarningWindowYesButtonGameObject.CreateButton(guiSkins.GetTexture(new RectInt(32, 64, 32, 32)), guiSkins.GetTexture(new RectInt(64, 64, 32, 32)), guiSkins.GetTexture(new RectInt(96, 64, 32, 32)), guiSkins.GetTexture(new RectInt(0, 64, 32, 32)));
            offlineWarningWindowYesButton.onClick.AddListener(() =>
            {
                ApplicationSettingsService.ShouldDisplayOfflineWarning = false;

                //Controller.GotoProfileSelectionComponent(this);
            });
        }

        private void Update()
        {
            if (_loginWindowBackgroundMaskBackgroundImage[1].transform.localPosition.y < 0)
            {
                _loginWindowBackgroundMaskBackgroundImage[0].transform.localPosition = Vector3.zero;
                _loginWindowBackgroundMaskBackgroundImage[1].transform.localPosition = new Vector3(0, 300, 0);
            }

            foreach (var image in _loginWindowBackgroundMaskBackgroundImage)
                image.transform.Translate(0, -Time.deltaTime * 50 * transform.localScale.magnitude, 0);

            if (!Input.GetKeyDown(KeyCode.Tab))
                return;

            if (_loginWindowUsernameInputField.isFocused)
                _loginWindowUsernameInputField.FindSelectableOnDown()?.Select();
            else if (_loginWindowTokenInputField.isFocused)
                _loginWindowTokenInputField.FindSelectableOnDown()?.Select();
        }
    }
}