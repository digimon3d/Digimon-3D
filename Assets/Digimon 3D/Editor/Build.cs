﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Digimon_3D.Editor
{
    public static class Build
    {
        [MenuItem("Digimon 3D/Build/Debug")]
        public static void BuildGameDebug()
        {
            BuildGame("Debug");
        }

        [MenuItem("Digimon 3D/Build/Windows x64 Build")]
        public static void BuildGame64()
        {
            BuildGame("Windows x64");
        }

        [MenuItem("Digimon 3D/Build/Build All")]
        public static void BuildGameAll()
        {
            BuildGame("Windows x64", "Debug");
        }

        private static void BuildGame(params string[] buildVersion)
        {
            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.Standalone, ApiCompatibilityLevel.NET_4_6);

            foreach (var version in buildVersion)
            {
                var buildPath = Path.GetFullPath(Path.Combine(Application.dataPath, "..", "Bin", version, "Digimon 3D.exe"));
                var scenesToBuild = new List<string>();

                foreach (var scene in EditorBuildSettings.scenes)
                {
                    if (scene.enabled)
                        scenesToBuild.Add(scene.path);
                }

                if (version.Equals("Windows x64", StringComparison.OrdinalIgnoreCase))
                    BuildPipeline.BuildPlayer(new BuildPlayerOptions { scenes = scenesToBuild.ToArray(), locationPathName = buildPath, targetGroup = BuildTargetGroup.Standalone, target = BuildTarget.StandaloneWindows64, options = BuildOptions.CompressWithLz4HC });
                else if (version.Equals("Debug", StringComparison.OrdinalIgnoreCase))
                    BuildPipeline.BuildPlayer(new BuildPlayerOptions { scenes = scenesToBuild.ToArray(), locationPathName = buildPath, targetGroup = BuildTargetGroup.Standalone, target = BuildTarget.StandaloneWindows64, options = BuildOptions.Development });
            }
        }
    }
}